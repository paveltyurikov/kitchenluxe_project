import React from 'react'


import {Route, Switch} from 'react-router-dom';
import Loadable from "react-loadable";
import Loading from 'components/Loading'
import {withAuthentication} from './apps/Auth'
import NotFound from "./components/NotFound";

//import asyncComponent from 'components/AsyncComponent/index'

export const AuthView = Loadable({
    loader: () => import(/* webpackChunkName: "app.auth.common" */ './apps/Auth'),
    loading: Loading,
});
export const ProductsView = Loadable({
    loader: () => import(/* webpackChunkName: "app.products.common" */ './apps/Products'),
    loading: Loading,
});

export const BasketView = Loadable({
    loader: () => import(/* webpackChunkName: "app.basket.common" */ './apps/Basket'),
    loading: Loading,
});

export const HomePage = Loadable({
    loader: () => import(/* webpackChunkName: "app.basket.common" */ './apps/HomePage'),
    loading: Loading,
});

export const CheckOut = Loadable({
    loader: () => import(/* webpackChunkName: "app.basket.common" */ './apps/CheckOut'),
    loading: Loading,
});
const Routes = ({basket, BasketLines}) => (
    <Switch>
        <Route exact path="/" component={HomePage}/>
        <Route path="/auth" component={withAuthentication(AuthView)}/>
        <Route exact path="/cart" component={BasketView}/>
        <Route exact path="/checkout" component={withAuthentication(CheckOut)}/>
        {/*<Route path="/checkout/success" render={() => <CheckOutSuccess basket={basket}/>}/>*/}
        <Route exact path="/products/:series?"
               render={(match) => <ProductsView {...match} basket={basket} basketLines={BasketLines}/>}/>

        <Route component={NotFound}/>
    </Switch>
)
export default Routes