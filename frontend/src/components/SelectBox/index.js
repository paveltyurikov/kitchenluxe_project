import React, {Component} from 'react';
import SelectBoxOption from './Option'
import './index.css'


class SelectBox extends Component {
    state = {
        selected: null,
    };

    checkOptionSelected = (option) => {
        return option.id === this.state.selected
    };

    onOptionClick = (id) => {
        this.setState({selected: id})
    };

    render() {
        const {options} = this.props;
        return (
            <div className="select-box">
                {
                    options && SelectBox.options.length > 0 &&
                    options.map(option =>
                        <SelectBoxOption
                            key={option.id}
                            onClick={() => this.onOptionClick(option.id)}
                            selected={this.checkOptionSelected(option.id)}
                            option={option}/>
                    )
                }
            </div>
        )
    }
}

export default SelectBox;