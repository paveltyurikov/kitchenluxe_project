import React from 'react'

const errorText = 'SelectBoxOption object should have label attr'

const SelectBoxOption = ({option, selected}) => (
    <div id={`sb_option_${option.id}`} className={`select-box-option ${selected ? 'selected' : ''}`}>
        <div className="select-box-option-label">{option.label ? option.label : errorText}</div>
    </div>
)

export default SelectBoxOption