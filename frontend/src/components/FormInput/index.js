import React from 'react';
import PropTypes from 'prop-types'
import './index.css'

const FormInput = ({children, className, inline}) => (
    <div className={`form-input${inline?'-inline':''} ${className ? className : ''}`}>{children}</div>
);

FormInput.propTypes = {
    className: PropTypes.string,
    inline: PropTypes.bool,
    children: PropTypes.any
};

export default FormInput;