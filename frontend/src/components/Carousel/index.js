import React, {Component} from 'react'
import Button from 'components/Button'
import './index.css'

class Carousel extends Component {
    state = {
        items: [],
        current: 0,
        previous: null,
        next: 1,
        show: true,
    };

    componentDidMount() {
        if (this.state.items.length === 0) {
            this.setState({items: this.props.items})
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.state.items.length === 0) {
            this.setState({items: nextProps.items})
        }
    }

    switchItem = (direction) => {
        const {items, current} = this.state;
        if (current !== null) {
            const nexCurrent = current + direction;
            const next = nexCurrent < items.length ? nexCurrent + 1 : null;
            const previous = nexCurrent >= 1 ? nexCurrent - 1 : null;
            this.setState({current: nexCurrent, next: next, previous: previous, show: true});
        }
    };
    switchForward = () => {
        this.state.next !== null && this.switchItem(1)
    };

    switchBack = () => {
        this.state.previous !== null && this.switchItem(-1)
    };

    render() {
        const {items, next, previous, current} = this.state;
        const {Component} = this.props;
        console.log()
        return (


            <div className={'carousel'}>


                {
                    items.length > 0 &&
                    items.map((item, index) => (
                            <div
                                key={index}
                                className={
                                    `carousel-item ${index === current ? 'current' : ''}`
                                }>
                                <Component   {...item}/>
                            </div>
                        )
                    )
                }
                <div className="carousel-controls">
                    <div className="btn"
                            onClick={this.switchBack}
                            dangerouslySetInnerHTML={{__html: '&#8678;'}}/>
                    <div className="btn"
                            onClick={this.switchForward}
                            dangerouslySetInnerHTML={{__html: '&#8680;'}}/>
                </div>
            </div>
        )
    }
}

export default Carousel;