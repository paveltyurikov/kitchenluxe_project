import React from 'react';
import Select from 'react-select';
import FormInput from '../FormInput';
import './index.css'

const ReactSelect = ({
                         options,
                         id,
                         className,
                         placeholder,
                         value,
                         onChange,
                         onBlur,
                         error,
                         label,
                         touched,
                         disabled,
                         clearable
                     }) => {
    const input = (
        <Select
            value={value}
            options={options}

            id={id}
            placeholder={placeholder}
            disabled={disabled}
            onChange={onChange}
            onBlur={onBlur}
            className={`${className ? className : ''} ${error && touched ? `error` : ''}`}
            clearable={clearable}
        />
    );
    return label ?
        (
            <FormInput>
                <label>
                    {label}
                    {input}
                    {error &&
                    touched && <div className="input-feedback">{error}</div>}
                </label>
            </FormInput>
        )
        :
        (
            <FormInput>
                {input}
                {error &&
                touched && <div className="input-feedback">{error}</div>}
            </FormInput>
        )
};

export default ReactSelect;