import React from 'react';
import './index.css'

const Container = ({children, className}) => (
    <div className={`container ${className ? className : ''}`}>{children}</div>
);

export default Container;