import React, {Component} from 'react';
import Button from 'components/Button'
import LoginForm from './components/Form/index'

class AuthForm extends Component {

    handleChange = (e) => {
        this.setState({[e.target.name]: e.target.value})
    };

    authLogin = () => {
        this.props.authLogin(this.state);
    };

    render() {
        return (
            <div className={'auth-view'}>
                <div className={'auth-view-body'}>
                    <div className="auth-icon"/>
                    <LoginForm
                        onChange={this.handleChange}
                        formAction={this.authLogin}
                    />
                </div>
            </div>
        )
    }
}


export default AuthForm;