import React from 'react'
import Button from 'components/Button'
import PropTypes from 'prop-types'
import * as messages from '../../messages';

const lang = localStorage.getItem('')
const LoginForm = ({onChange, formAction, error}) => (

    <form className={'login-form'}>
        <label>Login</label>
        <div className={'form-input'}>
            <input
                name={'username'}
                type={'text'}
                onChange={onChange}/>
        </div>
        <div className={'form-input'}>
            <label>Password</label>
            <input
                name={'password'}
                type={'password'}
                onChange={onChange}/>
        </div>
        {
            error &&
            <div className={'form-input'}>
                <p>{error}</p>
            </div>
        }

        <Button
            className="bnt-login"
            onClick={formAction}>Login</Button>
    </form>
);

LoginForm.propTypes = {
    onChange: PropTypes.func.isRequired,
    formAction: PropTypes.func.isRequired,
};

export default LoginForm;