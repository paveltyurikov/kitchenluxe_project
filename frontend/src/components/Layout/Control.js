import React from 'react'

const Control = (props) => (
    <span
        onClick={props.onClick}
        className={`control ${props.className}`}
    >
        {props.children}</span>
);

export default Control;