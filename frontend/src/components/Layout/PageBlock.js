import React from 'react'

const PageBlock = ({
                       className,
                       background_image,
                       background_color,
                       text,
                       inverse,
                   }) => (
    <div
        className={`page-block ${className ? className : ''}`}
        style={{
            backgroundImage: `url(${background_image})`,
            backgroundColor: background_color
        }}
    >
        <div
            className={`page-block-content ${inverse ? 'inverse' : ''}`}
            dangerouslySetInnerHTML={{__html: text}}
        />
    </div>
);

export default PageBlock;