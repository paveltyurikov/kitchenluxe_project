import React, {Fragment} from 'react'
import Header from '../Header';
import ContentSection from '../Content'
import Container from '../Container'
import './index.css'

const Layout = ({children}) => {
    return (
        <Fragment>
            <Header/>
            <ContentSection>
                <Container>
                    {children}
                </Container>
            </ContentSection>
        </Fragment>
    )
};

export default Layout;