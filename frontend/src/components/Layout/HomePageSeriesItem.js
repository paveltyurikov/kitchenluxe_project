import React from 'react';
import Fade from '../TransitionFade/index'
import {Link} from 'react-router-dom'

const HomePagesSeriesItem = ({image, name, description, slug}) => {
    return (

            <div className={'series-item'}>
                <div
                    style={
                        {
                            backgroundImage: `url(${image})`,
                        }
                    }
                    className={'series-item-image'}/>
                <div className={'series-item-title'}><Link to={`/products/${name}`}>{name}</Link></div>
                <div className={'series-item-text'}>{description}</div>
            </div>

    )
}
export default HomePagesSeriesItem;