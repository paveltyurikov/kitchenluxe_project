import React from 'react';
import './index.css'

const Hamburger = ({onClick, opened}) => (
    <div onClick={onClick} id="hamburger" className={`${opened ? 'opened':''}`}>
        <span/>
        <span/>
        <span/>
    </div>
);

export default Hamburger;