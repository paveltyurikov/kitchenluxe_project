import React from 'react';
import Control from '../Layout/Control'

const BasketLine = ({
                        index,
                        url,
                        product,
                        deleteBasketLine,
                        editBasketLine,
                        attributes,
                        edge,
                        width,
                        height,
                        quantity,
                        price_incl_tax,
                        line_total_price_incl_tax,
                        price_currency
                    }) => {
    return (<div
        className="basket-line"
        style={{
            backgroundImage: product.images[0] &&
            product.images[0].original && `url(${product.images[0].product_list_253_380})`
        }}

    >
        <div className={'controls'}>
            <Control
                onClick={() => deleteBasketLine(url, index)}
            >
                remove
            </Control>
            {/*<Control*/}
            {/*onClick={() => editBasketLine(index)}*/}
            {/*>edit</Control>*/}
        </div>
        <div className="basket-line-title">
            {product.title}
        </div>
        <div className="basket-line-meta">
            <div className="basket-line-attrs">
                {edge}
            </div>
            <div className="basket-line-size">
                {`${width} x ${height}`}
            </div>
            <div className="basket-line-quantity">
                {`x ${quantity}`}
            </div>
            <div className="basket-line-price">
                {line_total_price_incl_tax} {price_currency}
            </div>
        </div>
    </div>)
};

export default BasketLine;