import React, {Component} from 'react';

import BasketLine from './Line'

const BasketLines = ({
                                lines,
                                deleteBasketLine,
                                editBasketLine,
                                selectedProduct
                            }) => {
    const lines = selectedProduct && basketLines.length > 0 ?
        basketLines.filter(line => line.product.id === selectedProduct.id)
        :
        lines
    ;

    return (
        <div className="basket">
            {
                lines.length > 0 &&
                lines.map((line) => (
                        <BasketLine
                            key={line.url}
                            deleteBasketLine={deleteBasketLine}
                            editBasketLine={editBasketLine}
                            {...line}
                        />
                    )
                )
            }
        </div>
    )
};
