import React from 'react'
import {Link} from 'react-router-dom'

const BasketEmpty = () => (
    <div className={'cart-empty text-center'}>
        <h1>Your cart is empty</h1>
        <Link to={'/products'}>Back to catalog</Link>
    </div>);

export default BasketEmpty;