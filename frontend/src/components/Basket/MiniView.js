import React from 'react';
import BasketLine from './Line'

const BasketMiniView = ({lines, deleteBasketLine, editBasketLine}) => ([
    <div key="3001" className="basket-title">Basket</div>,
    lines.map((line, index) => <BasketLine
        index={index}
        key={index}
        deleteBasketLine={deleteBasketLine}
        editBasketLine={editBasketLine}
        {...line} />)
]);

export default BasketMiniView;