import React, {Component, Fragment} from 'react';
import './index.css'

export default class DropDown extends Component {
    state = {
        opened: this.props.opened
    };
    handleClick = () => {
        this.setState({opened: !this.state.opened})
    };

    render() {
        const {component, children, inline, className} = this.props;
        const {opened} = this.state;
        return (
            <Fragment>
                <div
                    className={`drop-down${inline ? '-inline' : ''} ${className ? className : ''}`}>
                    <div className="drop-down-wrap">
                        {component}
                        <button onClick={this.handleClick} className="drop-down-btn"/>
                    </div>

                </div>
                <div className={`drop-down-content ${opened ? 'opened' : ''}`}>
                    {children}
                </div>
            </Fragment>
        )
    }
}