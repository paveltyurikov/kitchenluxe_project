import React from 'react';

const SeriesView = ({series, fetchSeriesProducts, seriesName}) => {
    // const style = {
    //     backgroundImage: item.image &&
    //     `url(${item.image})`
    // };
    return (
        <div className={'product-series'}>
            {/*<div className={'controls'}>*/}
                {/*<span*/}
                    {/*onClick={switchSeriesVisible}*/}
                    {/*className={'control'}>close</span>*/}
            {/*</div>*/}
            {series &&
            series.map(item => {

                return (
                    <div
                        key={item.id}
                        onClick={() => fetchSeriesProducts(item.name)}
                        className={seriesName===item.name?'product-series-item active': 'product-series-item'}>
                        <div className={'product-series-item-title'}>{item.name}</div>
                    </div>
                )
            })
            }

        </div>
    )
}

export default SeriesView;