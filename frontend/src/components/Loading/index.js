import React from 'react'
import Loader from 'components/Loader'
import LoaderRings from 'components/LoaderRings'
import './index.css'

const Loading = () => (
    <div className="centered-block loading-c">
        <LoaderRings size={150} stroke="#ffffff"/>
        <div className="loading-c-title">Загрузка...</div>
    </div>
);

export default Loading;