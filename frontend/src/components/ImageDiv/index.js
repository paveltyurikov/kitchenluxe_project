import React from 'react';

const ImageDiv = ({image, className}) => {
    return (
        <div className={`image-div ${className}`}
        style={{
            backgroundImage: `url(${image})`
        }}/>
    )
};

export default ImageDiv;