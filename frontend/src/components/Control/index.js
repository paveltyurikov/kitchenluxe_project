import React from 'react'

const Control = ({className, children, onClick}) => (
    <span onClick={onClick} className={`control ${className ? className : ''}`}>{children}</span>
);

export default Control;