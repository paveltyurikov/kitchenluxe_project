import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';
import DropDown from 'components/DropDown'
import './index.css'


class Nav extends Component {
    render() {
        return (
            <nav className={'top-nav'}>
                <div className={'top-nav-primary'}>
                    <NavLink exact to="/" className="nav-link">Home</NavLink>
                    <NavLink to="/products" className="nav-link">Products</NavLink>
                    <NavLink exact to="/cart" className="nav-link">Cart</NavLink>
                </div>
                <div className={'top-nav-primary'}>

                    <NavLink exact to="/auth" className="nav-link">Login</NavLink>
                </div>
            </nav>
        )
    }
};


export default Nav;