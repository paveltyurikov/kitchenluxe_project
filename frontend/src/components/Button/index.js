import React from "react"
import './index.css'

const Button = ({className, onClick, type, disabled, children}) => (
    <button
        type={type ? type : "button"}
        disabled={disabled}
        className={`btn ${className ? className : ''}`}
        onClick={onClick}
    >
        {children}
    </button>
);

export default Button