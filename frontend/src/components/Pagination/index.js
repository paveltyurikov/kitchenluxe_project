import React, {Component} from 'react';
import Button from 'components/Button'
import './index.css'


const Pagination = ({className, next, previous, fetchPage}) => {
    return (
        <div className={`pagination ${className ? className : ''}`}>
            <div className="pagination-controls">
                {
                    previous &&
                    <Button
                        className="pagination-previous"
                        onClick={() => fetchPage(previous)}
                    >
                        previous
                    </Button>
                }
                {
                    next &&
                    <Button
                        className="pagination-next"
                        onClick={() => fetchPage(next)}
                    >
                        next
                    </Button>
                }
            </div>
        </div>
    )
};

export default Pagination;