import React from 'react';

const Footer = ({children, common})=>(
    <footer>
        {children}
        <div className="footer-common">{common}</div>
    </footer>
);

export default Footer;