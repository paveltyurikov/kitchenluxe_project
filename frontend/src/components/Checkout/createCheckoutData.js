export const createCheckoutData = ({basket, guest_email, first_name, phone_number, notes}) => {
    //console.log(basket)
    return {
        basket: basket.url,
        guest_email: guest_email,
        total: basket.total_incl_tax,
        shipping_method_code: "no-shipping-required",
        shipping_charge: {
            currency: basket['currency'],
            excl_tax: "0.0",
            tax: "0.0"
        },
        shipping_address: {
            country: "http://localhost:9006/api/countries/US/",
            first_name: first_name,
            last_name: "",
            line1: "No address",
            line2: "",
            line3: "",
            line4: "",
            notes: notes,
            phone_number: phone_number,
            //postcode: "7777KK",
            //state: "Gerendrecht",
            // title: "Mr"
        }
    }
}