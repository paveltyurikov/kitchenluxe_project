import React, {Component} from "react";
import {createCheckoutData} from "./createCheckoutData";
import {Redirect} from 'react-router-dom';
import Input from '../Form/Input'
import TextArea from '../Form/TextArea'

class CheckOutFormTemplate extends Component {

    handleChange=(e)=> {
        this.setState({[e.target.name]: e.trget.value})
    };

    onSubmit=(e)=> {
        e.preventDefault();
        const vals=this.props.values;
        vals.basket = this.props.basket.basket;
        this.props.checkoutBasket(createCheckoutData({...vals}))
    };

    render() {

        const {
            values,
            errors,
            touched,
            isSubmitting,
            handleChange,
            handleBlur,
            checkoutData,
        } = this.props;
        if (Object.keys(checkoutData).length>0){return  (<Redirect to={'/checkout/success'}/>);}
        return (
            <div className="checkout-view-form">
                <form onSubmit={this.onSubmit} className="checkout-view-form-body">
                    <div className={'form-inputs'}>
                        <Input
                            id="first_name"
                            type="text"
                            name={'first_name'}
                            value={values.first_name}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.first_name}
                            error={errors.first_name}
                            disabled={isSubmitting}
                            placeholder={"Name"}
                        />
                        <Input
                            id="guest_email"
                            type="text"
                            name={'guest_email'}
                            value={values.guest_email}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.guest_email}
                            error={errors.guest_email}
                            disabled={isSubmitting}
                            placeholder={"Email"}
                        />
                        <Input
                            id="phone_number"
                            type="text"
                            name={'phone_number'}
                            value={values.phone_number}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.phone_number}
                            error={errors.phone_number}
                            disabled={isSubmitting}
                            placeholder={"Phone Number"}
                        />

                        <Input
                            id="line1"
                            type="text"
                            name={'line1'}
                            value={values.line1}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.line1}
                            error={errors.line1}
                            disabled={isSubmitting}
                            placeholder={"Address Line 1"}
                        />
                        <Input
                            id="line2"
                            type="text"
                            name={'line2'}
                            value={values.line2}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.line2}
                            error={errors.line2}
                            disabled={isSubmitting}
                            placeholder={"Address Line 2"}
                        />
                        <Input
                            id="line3"
                            type="text"
                            name={'line3'}
                            value={values.line3}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.line3}
                            error={errors.line3}
                            disabled={isSubmitting}
                            placeholder={"Address Line 3"}
                        />
                        <Input
                            id="line4"
                            type="text"
                            name={'line4'}
                            value={values.line4}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.line4}
                            error={errors.line4}
                            disabled={isSubmitting}
                            placeholder={"Address Line 4"}
                        />
                        <TextArea
                            id="notes"
                            name={'notes'}
                            value={values.notes}
                            onChange={handleChange}
                            onBlur={handleBlur}
                            touched={touched.notes}
                            error={errors.notes}
                            disabled={isSubmitting}
                            placeholder={"Notes"}
                        />
                    </div>
                    <button className="checkout-view-form-submit" type="submit">Checkout</button>
                </form>
            </div>
        )
    }

};

export default CheckOutFormTemplate;