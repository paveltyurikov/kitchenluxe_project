import React, {Component} from 'react';

class ImageLoadable extends Component {

    state = {
        src: '/static/images/no-image.png',
        loaded: false
    };

    componentDidMount() {
        const {src} = this.props; // get image primary src
        const imageLoadable = new Image(); // create an image object programmatically;
        imageLoadable.onload = () => this.setState({src: src, loaded: true});
        imageLoadable.src = src // do it after you set onload handler
    }

    render() {
        const {src, loaded} = this.state;
        const {className} = this.props;
        const imageStyle = {
            backgroundImage: `url(${src})`
        };
        return (
            <div
                style={imageStyle}
                className={`${className ? className : ''} image-loadable
                ${loaded ? ' loaded' : ' loading'}`}
            />
        )
    }
}

export default ImageLoadable