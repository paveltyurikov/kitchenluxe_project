import React, {Component} from 'react';
import {withFormik} from 'formik';
import Field from "../Form/Field";
import Button from "../Button";
import scrollToFirstError from "../Form/scrollToFirstError";

const getInitialFormValues = ({formInitialValues, fields}) => {
    const genFormInitialValues = {};
    fields.forEach(field => {
        genFormInitialValues[field.name] = field.defaultValue ? field.defaultValue : "";
    });
    return formInitialValues ? formInitialValues : genFormInitialValues
};


class FormikForm extends Component {
    state = {
        formInitialValues: {},
        fields: [],
    };

    componentDidMount() {
        const {fields} = this.props;
        this.setState({fields: fields});
    }

    componentDidUpdate(prevProps) {
        // if form was submitting, but now is not submitting because it is invalid
        if (prevProps.isSubmitting && !this.props.isSubmitting && !this.props.isValid) {
            scrollToFirstError(this.props.errors)
        }
    }

    componentWillReceiveProps(newProps) {

        const {requestErrors, setErrors, clearRequestErrors} = newProps;
        if (requestErrors && Object.keys(requestErrors).length > 0) {
            clearRequestErrors();
            setErrors(newProps.requestErrors);

            //scrollToFirstError(newProps.requestErrors)
        }
    };

    handleImageFieldChange = (e) => {
        const {handleChange, handleImageFieldChange} = this.props;
        handleChange(e);
        handleImageFieldChange(e)
        // let file = e.target.files[0];
        // this.props.setFieldValue('file', file);
    };

    handleSelectChange = (field, value) => value && this.props.setFieldValue(field.name, value.value);


    getChangeHandlerForField = (field) => {
        const {fieldType} = field;
        if (fieldType) {
            switch (fieldType) {
                case "select":
                    return (value) => this.handleSelectChange(field, value)
                case "image":
                    return this.handleImageFieldChange
                default:
                    return this.props.handleChange
            }
        }
        return this.props.handleChange
    };

    render() {
        const {
            className,
            formSubmitButtonText = "Отправить",
            // Formik's props
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            //isSubmitting,
        } = this.props;
        const {fields} = this.state;
        return values ? (
                <form className={className} onSubmit={handleSubmit}>
                    {
                        fields.map((field) =>
                            <Field
                                key={field.name}
                                fieldType={field.fieldType}
                                id={field.name}
                                placeholder={field.placeholder}
                                value={values[field.name]}
                                onChange={this.getChangeHandlerForField(field)}
                                onBlur={handleBlur}
                                touched={touched[field.name]}
                                error={errors[field.name]}
                                {...field}
                            />
                        )
                    }
                    <div className="form-controls">
                        <Button type="submit">
                            {formSubmitButtonText}
                        </Button>
                    </div>
                </form>

            )
            : (<div>submitting</div>)
    }
}

export default withFormik({
    mapPropsToValues: props => getInitialFormValues({...props}),
    validationSchema: props => props.formValidationSchema,
    handleSubmit: (values, {props, ...other}) => props.formSubmit(values, {...other}),
    displayName: props => props.displayName, // helps with React DevTools
})(FormikForm);
// Usage
// <FormikForm
//     className="centered-block"
//     fields={fields}
//     formSubmit={this.formSubmit}
//     formInitialValues={formInitialValues}
//     formValidationSchema={formValidationSchema}
//     displayName="Login Form"
//     formSubmitButtonText="Login"
// />
