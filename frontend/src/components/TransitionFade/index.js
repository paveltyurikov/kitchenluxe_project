import React from 'react';
import Transition from 'react-transition-group/Transition';

const duration = 300;

const defaultStyle = (duration) => ({
    transition: `opacity ${duration}ms ease-in-out`,
    opacity: 0,
})

const transitionStyles = {
    entering: {opacity: 0},
    entered: {opacity: 1},
};

const Fade = ({in: inProp, duration:durationProp, children}) => (
    <Transition in={inProp} timeout={durationProp}>
        {(state) => (
            <div style={{
                ...defaultStyle(durationProp),
                ...transitionStyles[state]
            }}>
                {children}
            </div>
        )}
    </Transition>
);
export default Fade;