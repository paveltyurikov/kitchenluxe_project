import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

const Nav = ({switchSeriesVisible}) => {
        return (
            <nav className={'top-nav'}>
                <span onClick={() => switchSeriesVisible()} className={'nav-link'}>Series</span>
                <NavLink to="/products" className={'nav-link'}>Products</NavLink>
                <NavLink to="/cart" className={'nav-link'}>Basket</NavLink>
                <NavLink to="/checkout" className={'nav-link'}>Checkout</NavLink>
            </nav>
        )
    }
;

export default Nav;