import React from 'react';
import './index.css'
const Content = ({className, children})=>(
    <section id="content" className={className}>{children}</section>
)
export default Content