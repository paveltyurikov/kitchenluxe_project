import React from 'react';

const CheckBox = (props) => {
    return (
        <div className="checkobox">
            <label>
                <div className="inlineBlock check">
                    <input type="checkbox" id={props.id} name={props.name} value={props.value}/>
                </div>
                <div className="inlineBlock">{props.label}</div>
            </label>
        </div>
    )
}
export default CheckBox;