import React from 'react';

const FormInput = ({className, children}) => (
    <div className={`form-input ${className ? className: ''}`}>{children}</div>
);
export default FormInput;