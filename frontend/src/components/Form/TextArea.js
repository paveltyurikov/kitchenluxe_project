import React from 'react';

const TextArea = ({
                      additionalClasses,
                      id,
                      placeholder,
                      value,
                      onChange,
                      onBlur,
                      error,
                      label,
                      touched,
                      disabled
                  }) => {
    const input = (<textarea
        id={id}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        disabled={disabled}
        onBlur={onBlur}
        className={error && touched ? `text-input error` : 'text-input'}
    />);
    if (label) {
        return (
            <div className={`${additionalClasses} form-input`}>
                <label>
                    {label}
                    {input}
                    {error &&
                    touched && <div className="input-feedback">{error}</div>}
                </label>
            </div>
        )

    }
    return (
        <div className={`${additionalClasses} form-input`}>
            {input}
            {error &&
            touched && <div className="input-feedback">{error}</div>}
        </div>
    )


}
export default TextArea;