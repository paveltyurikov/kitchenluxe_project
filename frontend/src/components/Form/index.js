import React from 'react';
import PropTypes from 'prop-types'
import './index.css'

const Form = ({children, className, inline, onSubmit, onReset}) => (
    <form
        className={`form${inline?'-inline':''} ${className ? className : ''}`}
        onSubmit={onSubmit}
        onReset={onReset} >
        {children}
    </form>
);

Form.propTypes = {
    className: PropTypes.string,
    inline: PropTypes.bool,
    onSubmit: PropTypes.func,
    onReset: PropTypes.func,
    children: PropTypes.any
};

export default Form;