import React from 'react';
import Select from 'react-select';
import '../../ReactSelect/index.css'

const ReactSelect = ({
                         options,
                         id,
                         className,
                         placeholder,
                         value,
                         onChange,
                         onBlur,
                         error,
                         label,
                         touched,
                         disabled,
                         clearable,
                         defaultValue

                     }) => (
    <Select
        value={value}
        options={options}
        id={id}
        placeholder={placeholder}
        disabled={disabled}
        onChange={onChange}
        onBlur={onBlur}
        className={`${className ? className : ''} ${error && touched ? `error` : ''}`}
        clearable={clearable}
        defaultValue={defaultValue}
    />
);

export default ReactSelect