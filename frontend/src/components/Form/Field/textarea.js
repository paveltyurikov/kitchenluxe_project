import React from 'react';

const TextArea = ({
                         id,
                         className,
                         placeholder,
                         type,
                         value,
                         onChange,
                         onBlur,
                         error,
                         label,
                         touched,
                         disabled,
                     }) => (
    <textarea
        id={id}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        disabled={disabled}
        onBlur={onBlur}
        className={error && touched ? `text-input error` : 'text-input'}
    />
);

export default TextArea