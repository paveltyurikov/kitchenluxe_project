import React, {Component} from 'react';
import FormInput from 'components/FormInput'
import Input from './input'
import ImageInput from './image'
import MaskedInput from './inputmasked'
import ReactSelect from './select'
import TextArea from "./textarea";


class Field extends Component {
    getComponent = () => {
        switch (this.props.fieldType) {
            case "input":
                return Input;
            case "masked":
                return MaskedInput;
            case "select":
                return ReactSelect;
            case "textarea":
                return TextArea;
            case "image":
                return ImageInput;
            default:
                return Input;
        }
    };

    render() {
        const {label, error, touched} = this.props;
        const FiledInput = this.getComponent();
        return (
            <FormInput className={error && touched ? 'error' : null}>

                {label && <label>{label}</label>}

                <FiledInput {...this.props}/>

                {
                    error && touched &&
                    <div className="form-input-feedback error">{error}</div>
                }

            </FormInput>
        )
    }
}

export default Field
