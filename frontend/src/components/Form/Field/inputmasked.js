import React from 'react';
import InputMask from 'react-input-mask';

const MaskedInput = ({
                         id,
                         className,
                         placeholder,
                         type,
                         value,
                         onChange,
                         onBlur,
                         error,
                         label,
                         touched,
                         disabled,

                         mask = "+7 (999) 999-99-99",
                         maskChar = " "
                     }) => (
    <InputMask
        id={id}
        mask={mask}
        maskChar={maskChar}
        placeholder={placeholder}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        className={`df ${error && touched ? 'error' : ''}`}
        disabled={disabled}
    />
);

export default MaskedInput;