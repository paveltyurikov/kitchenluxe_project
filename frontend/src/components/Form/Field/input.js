import React from 'react';

const Input = ({
                   id,
                   name,
                   className,
                   placeholder,
                   type,
                   value,
                   onChange,
                   onBlur,
                   error,
                   touched,
                   disabled,
    autocomplete,
               }) => (
    <input
        id={id}
        placeholder={placeholder}
        disabled={disabled}
        type={type}
        value={value}
        onChange={onChange}
        onBlur={onBlur}
        className={`${className ? className : ''} ${error && touched ? `error` : ''}`}
        autoComplete={autocomplete}
    />
);

export default Input;