import React, {Fragment} from 'react';

const ImageInput = ({
                        id,
                        className,
                        placeholder,
                        type,
                        value,
                        onChange,
                        onBlur,
                        error,
                        label,
                        touched,
                        disabled,
                    }) => (
    <Fragment>
        <input
            id={id}
            type="file"
            placeholder={placeholder}
            style={
                {
                    backgroundImage: `url(${value})`,
                }
            }
            //value={value}
            onChange={onChange}
            disabled={disabled}
            onBlur={onBlur}
            className={error && touched ? `text-input error` : 'text-input'}
        />
    </Fragment>
);

export default ImageInput;