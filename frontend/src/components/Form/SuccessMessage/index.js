import React from 'react'

const SuccessMessage = ({
                            message,
                            className,
                            centered
                        }) =>{
     console.log('success-message',message)
   return  (

    <div className={`success-message
        ${centered ? 'centered-block text-center' : ''}
        ${className ? className : ''}`} dangerouslySetInnerHTML={
        {
            __html: message
        }
    }/>
);
}

export default SuccessMessage