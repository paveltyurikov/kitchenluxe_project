import React from 'react';

const InputRadio = (props) => {
    return (
        <div className={ props.disabled ? 'radioInput disabled': 'radioInput'}>
            <input
                type="radio"
                id={props.id}
                name={props.name}
                value={props.value}
                checked={props.checked}
                onChange={props.onChange}
                onBlur={props.onBlur}
                className={props.errors && props.touched ? 'radio error' : 'radio'}
                disabled={props.disabled}
            />
            <label htmlFor={props.id}><span dangerouslySetInnerHTML={{__html: props.label}}/>
                {props.additionalText && <div className={'add-text'} >{props.additionalText}</div>}</label>
            {props.error &&
            props.touched && <div className="input-feedback">{props.error}</div>}
        </div>
    )
}
export default InputRadio;