 import {scroller} from 'react-scroll';

export default function scrollToFirstError(errors) {
    const first_error = Object.keys(errors)[0];

    if (first_error){
        scroller.scrollTo(first_error, {offset: -200, smooth: true, duration:400});
    }
}