import React from 'react';

const FormGroupInline = ({className, children}) => (
    <div className={`form-group-inline ${className ? className : ''}`}>{children}</div>
);
export default FormGroupInline;