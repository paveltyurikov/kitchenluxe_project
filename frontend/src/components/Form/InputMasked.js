import React from 'react';
import InputMask from 'react-input-mask';
import FormInput from '../FormInput';


const InputMasked = ({
                         id,
                         className,
                         placeholder,
                         type,
                         value,
                         onChange,
                         onBlur,
                         error,
                         label,
                         touched,
                         disabled,

                         mask = "+7 (999) 999-99-99",
                         maskChar = " "
                     }) => {
    const input = (
        <InputMask
            id={id}
            mask={mask}
            maskChar={maskChar}
            placeholder={placeholder}
            type={type}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            className={`text-input ${error && touched ? 'error' : ''}`}
            disabled={disabled}
        />
    );
    return label ?
        (
            <FormInput>
                <label>
                    {label}
                    {input}
                    {error &&
                    touched && <div className="input-feedback">{error}</div>}
                </label>
            </FormInput>
        )
        :
        (
            <FormInput>
                {input}
                {error &&
                touched && <div className="input-feedback">{error}</div>}
            </FormInput>
        )

}

// InputMasked.propTypes = {
//     id: PropTypes.string.isRequired,
//     type: PropTypes.string.isRequired,
//     mask: PropTypes.string.isRequired,
//     onChange: PropTypes.func.isRequired,
// };
export default InputMasked;