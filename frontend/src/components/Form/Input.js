import React from 'react';
import FormInput from 'components/FormInput'

const Input = ({
                   id,
                   className,
                   placeholder,
                   type,
                   value,
                   onChange,
                   onBlur,
                   error,
                   label,
                   touched,
                   disabled
               }) => {
    const input = (
        <input
            id={id}
            placeholder={placeholder}
            disabled={disabled}
            type={type}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            className={`${className ? className : ''} ${error && touched ? `error` : ''}`}
        />
    );
    return label ?
        (
            <FormInput>
                <label>
                    {label}
                    {input}
                    {error &&
                    touched && <div className="input-feedback">{error}</div>}
                </label>
            </FormInput>
        )
        :
        (
            <FormInput>
                {input}
                {error &&
                touched && <div className="input-feedback">{error}</div>}
            </FormInput>
        )
};
export default Input;