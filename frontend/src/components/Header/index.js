import React from 'react';
import Nav from '../Nav'
import Container from '../Container'
import './index.css'

const Header = () => (
    <header>
        <h1 className={'luxe-text text-center'}>Kitchen Luxe</h1>
        <Container>
            <div className="top-nav-wrap"><Nav/></div>
        </Container>
    </header>
);


export default Header;