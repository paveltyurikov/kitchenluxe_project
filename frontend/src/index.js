// import 'core-js/es6/map';
// import 'core-js/es6/set';
//import 'babel-polyfill';

import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import createHistory from 'history/createBrowserHistory';
import registerServiceWorker from './registerServiceWorker';
import App from "./apps/App/index";
import configureStore from './store'

const history = createHistory();

//Store
const store = configureStore({}, history);

ReactDOM.render(
    <Provider store={store}>
        <App history={history}/>
    </Provider>,
    document.getElementById('wrap')
);
registerServiceWorker();
