import {call, put, takeLatest} from 'redux-saga/effects'
import Api from './api'
import * as actionTypes from './actiontypes'
import * as actions from './actions'

import * as BasketActions from "apps/Basket/actions";

export function* fetchBasket(action) {
    try {
        const data = yield call(Api.fetchBasket, action);
        yield put(BasketActions.fetchBasketSuccess(data));
        yield put(BasketActions.fetchBasketLines(data.lines))
    } catch (error) {
        console.log(error.message);
        yield put(BasketActions.dataIsFetching(false))
    }
}


export function* fetchProducts(action) {
    try {
        const data = yield call(Api.fetchProducts, action);
        if (action.page) {
            yield put(actions.fetchProductsPageSuccess(data))
        }
        else {
            yield put(actions.fetchProductsSuccess(data))
        }
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}


export function* fetchProductDetails(action) {
    try {
        const data = yield call(Api.fetchProductDetails, action);
        yield put(actions.fetchProductDetailsSuccess(data))
    } catch (error) {
        console.log('fetchProductDetails', error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* fetchSeries(action) {
    try {
        const data = yield call(Api.fetchSeries, action);
        yield put(actions.fetchSeriesSuccess(data))

    } catch (error) {
        console.log('fetchSeries', error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* fetchSeriesProducts(action) {
    try {
        const data = yield call(Api.fetchSeriesProducts, action);
        yield put(actions.fetchProductsSuccess(data))
        yield put(actions.setSeries(action.series))
    } catch (error) {
        console.log('fetchSeriesProducts', error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* addProductToBasket(action) {
    try {
        yield put(actions.dataIsFetching(true))
        const data = yield call(Api.addProductToBasket, action);
        yield put(actions.addProductToBasketSuccess(data))
    } catch (error) {
        console.log('addProductToBasket', error.message);
        yield put(actions.dataIsFetching(false))
    }
}


export default function* productsSaga() {
    yield takeLatest(actionTypes.FETCH_PRODUCTS, fetchProducts);

    yield takeLatest(actionTypes.FETCH_PRODUCT_DETAILS, fetchProductDetails);
    yield takeLatest(actionTypes.FETCH_SERIES, fetchSeries);
    yield takeLatest(actionTypes.ADD_PRODUCT_TO_BASKET_SUCCESS, fetchBasket);
    yield takeLatest(actionTypes.FETCH_BASKET, fetchBasket);
    yield takeLatest(actionTypes.FETCH_SERIES_PRODUCTS, fetchSeriesProducts);
    yield takeLatest(actionTypes.ADD_PRODUCT_TO_BASKET, addProductToBasket);
}

