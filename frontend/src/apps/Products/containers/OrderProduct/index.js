import React, {Component} from 'react';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {withFormik} from 'formik';
import Form from 'apps/Basket/components/Form'
import {deleteBasketLine} from 'apps/Basket/actions'
import './index.css'

import * as actions from "../../actions";

const checkFiled = (field, errors, touched) => {
    return errors[field] && touched[field]
};


const AddToBasketForm = withFormik({
    // Transform outer props into form values
    mapPropsToValues: props => ({quantity: 1, edge: '', width: '', height: ''}),
    // Add a custom validation function (this can be async too!)
    validate: (values, props) => {
        const errors = {};
        Object.keys(values).map(key => {
                if (!values[key]) {
                    errors[key] = `Fill ${key} field, please`;
                }
            }
        );
        return errors;
    },
    // Submission handler
    handleSubmit: (values,
                   {
                       props,
                       setSubmitting,
                       setErrors /* setValues, setStatus, and other goodies */,
                   }) => {
    },
})(Form);

const mapStateToProps = (state) => (state.basket);

const mapDispatchToProps = dispatch => bindActionCreators(
    {...actions, deleteBasketLine}, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps)(AddToBasketForm)