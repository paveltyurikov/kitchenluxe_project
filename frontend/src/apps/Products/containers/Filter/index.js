import React from 'react';

import {connect} from "react-redux";
import ProductFilter from '../../components/Filter'


const mapStateToProps = (state) => (state.products);
export default connect(
    mapStateToProps,
    null)(ProductFilter)