import * as actiontypes from './actiontypes';
import {makeActionCreator} from 'api/index'

// const makeActionCreator = (type, ...argNames) => {
//     return function (...args) {
//         let action = {type};
//         argNames.forEach((arg, index) => {
//             action[argNames[index]] = args[index]
//         });
//         return action
//     }
// };


export const dataIsFetching = makeActionCreator(actiontypes.DATA_IS_FETCHING, 'bool');

export const fetchProducts = makeActionCreator(actiontypes.FETCH_PRODUCTS, 'page');
export const fetchProductsSuccess = makeActionCreator(actiontypes.FETCH_PRODUCTS_SUCCESS, 'data');
export const fetchProductsPageSuccess = makeActionCreator(actiontypes.FETCH_PRODUCTS_PAGE_SUCCESS, 'data');
export const fetchSeries = makeActionCreator(actiontypes.FETCH_SERIES);
export const setSeries = makeActionCreator(actiontypes.SET_SERIES, 'series');
export const fetchSeriesSuccess = makeActionCreator(actiontypes.FETCH_SERIES_SUCCESS, 'data');
export const fetchSeriesProducts = makeActionCreator(actiontypes.FETCH_SERIES_PRODUCTS, 'series');

export const fetchProductDetails = makeActionCreator(actiontypes.FETCH_PRODUCT_DETAILS, 'url');
export const fetchProductDetailsSuccess = makeActionCreator(actiontypes.FETCH_PRODUCT_DETAILS_SUCCESS, 'data');
export const selectProduct = makeActionCreator(actiontypes.SELECT_PRODUCT, 'id');
export const closeProduct = makeActionCreator(actiontypes.CLOSE_PRODUCT);
export const addProductToBasket = makeActionCreator(actiontypes.ADD_PRODUCT_TO_BASKET, 'url', 'quantity', 'options');
export const addProductToBasketSuccess = makeActionCreator(actiontypes.ADD_PRODUCT_TO_BASKET_SUCCESS, 'data');
export const fetchBasket = makeActionCreator(actiontypes.FETCH_BASKET);
export const switchSeriesVisible = makeActionCreator(actiontypes.SWITCH_SERIES_VISIBLE, 'bool');
