import React, {Component, Fragment} from 'react';
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import reducer from './reducer';
import saga from './saga';

import * as actions from "./actions";
import Footer from 'components/Footer'
import ProductList from './components/List/index'
import OrderProduct from './containers/OrderProduct/index'
import ProductDetails from './components/Details'
import Button from 'components/Button'


class ProductsView extends Component {
    componentDidMount() {
        const {match, fetchSeriesProducts, fetchProducts, fetchSeries} = this.props;
        const {series} = match.params;
        if (series) {
            fetchSeriesProducts(series)
        }
        else {
            fetchProducts();
        }
        fetchSeries();
    }

    fetchMore = () => {
        const {fetchProducts, next} = this.props;
        fetchProducts(next)
    };

    render() {
        const {
            next,
            previous,
            products,
            productDetails,
            selectedProduct,
            selectProduct,
            closeProduct,
            fetchProductDetails,
            addProductToBasket,
            fetchProducts
        } = this.props;


        return (

            <div className="products-section">
                {
                    selectedProduct &&
                    <OrderProduct
                        addProductToBasket={addProductToBasket}
                        closeProduct={closeProduct}
                        selectedProduct={selectedProduct}/>
                }

                {
                    products &&
                    <ProductList
                        products={products}
                        selectProduct={selectProduct}
                        fetchProductDetails={fetchProductDetails}
                    />
                }
                {
                    next &&
                    <div className="text-center"><Button onClick={this.fetchMore}>More</Button></div>
                }
                {
                    productDetails &&
                    <ProductDetails
                        closeProduct={closeProduct}
                        {...productDetails}/>
                }
                <Footer>

                    {/*<Pagination*/}
                    {/*next={next}*/}
                    {/*previous={previous}*/}
                    {/*fetchPage={fetchProducts}/>*/}
                </Footer>
            </div>

        )
    }
}

const mapStateToProps = state => (state.products);
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const withReducer = injectReducer({key: 'products', reducer});
const withSaga = injectSaga({key: 'products', saga});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect)(ProductsView)
