import * as actionTypes from './actiontypes';
import {createReducer} from 'api/index'

const productsInitialState =
    {
        products: null,
        categories: [],
        seriesVisible: false,
    };

const productsReducer = createReducer(productsInitialState, {

    [actionTypes.DATA_IS_FETCHING](state, action) {
        return {
            ...state,
            dataIsFetching: action.bool
        };
    },
    [actionTypes.FETCH_PRODUCTS](state, action) {
        return {
            ...state,
            dataIsFetching: true,
        };
    },
    [actionTypes.FETCH_PRODUCTS_SUCCESS](state, action) {
        return {
            ...state,
            products: action.data.results,
            count: action.data.count,
            previous: action.data.previous,
            next: action.data.next,
            seriesVisible: false,
            dataIsFetching: false,
        };
    },
    [actionTypes.FETCH_PRODUCTS_PAGE_SUCCESS](state, action) {
        const products = [...state.products, ...action.data.results];
        return {
            ...state,
            products: products,
            count: action.data.count,
            previous: action.data.previous,
            next: action.data.next,
            seriesVisible: false,
            dataIsFetching: false,
        };
    },

    [actionTypes.SELECT_PRODUCT](state, action) {

        return {
            ...state,
            selectedProductIndex: action.id,
            selectedProduct: state.products.filter(product => product.id === action.id)[0]

        };
    },
    [actionTypes.CLOSE_PRODUCT](state, action) {

        return {
            ...state,
            selectedProductIndex: null,
            selectedProduct: null,
            productDetails: null,
        };
    },
    [actionTypes.FETCH_PRODUCT_DETAILS_SUCCESS](state, action) {
        return {
            ...state,
            productDetails: action.data
        };
    },
    [actionTypes.FETCH_SERIES_SUCCESS](state, action) {
        const categories = [];
        action.data.forEach(obj => {
            if (obj.depth === 1) {
                const category = {...obj};
                category.subCategories = action.data
                    .filter(subCategory => subCategory.parent === obj.id)
                    .map(child => ({...child, parentName: category.name}));
                categories.push(category)
            }
        });
        return {
            ...state,
            categories: categories,
        };
    },
    [actionTypes.SET_SERIES](state, action) {
        return {
            ...state,
            seriesName: action.series,
        };
    },
    [actionTypes.SWITCH_SERIES_VISIBLE](state, action) {
        return {
            ...state,
            seriesVisible: !state.seriesVisible,
        };
    },


});

export default productsReducer;
