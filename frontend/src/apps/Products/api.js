
import {getHeaders} from 'api/index'

export const FETCH_PRODUCTS = '/api/cat/';
export const FETCH_BASKET = '/api/basket/';
export const FETCH_SERIES = '/api/series/';
export const ADD_PRODUCT_TO_BASKET = '/api/basket/add-product/';

const Api = {
    fetchProducts: async (action) => {
       const url = action.page || FETCH_PRODUCTS;
        const fetchInit = {
            method: 'GET',
            headers: getHeaders(),
            credentials: "same-origin"
        };
        const response = await fetch(url, fetchInit);
        return await response.json();
    },
    fetchSeriesProducts: async (action) => {
        const url = `${FETCH_PRODUCTS}?search=${action.series}` || FETCH_PRODUCTS;
        const fetchInit = {
            method: 'GET',
            headers: getHeaders(),
            credentials: "same-origin"
        };
        const response = await fetch(url, fetchInit);
        return await response.json();
    },
    fetchProductDetails: async (action) => {
        const fetchInit = {
            method: 'GET',
            headers: getHeaders(),
            credentials: "same-origin"
        };
        const response = await fetch(action.url, fetchInit);
        return await response.json();
    },
    addProductToBasket: async (action) => {

        const basketline = {
            url: action.url,
            quantity: action.quantity,
            options: action.options
        };
        //console.log('basketline', basketline, JSON.stringify(basketline));
        const response = await fetch(
            ADD_PRODUCT_TO_BASKET,

            {
                method: 'POST',
                headers: getHeaders(),
                credentials: "same-origin",
                body: JSON.stringify(basketline)
            });

        return await response.json();
    },
    fetchSeries: async (action) => {
        const url = FETCH_SERIES;
        const fetchInit = {
            method: 'GET',
            headers: getHeaders(),
            credentials: "same-origin"
        };
        const response = await fetch(url, fetchInit);
        return await response.json();
    },
    fetchBasket: async () => {
        const fetchInit = {
            method: 'GET',
            headers: getHeaders(),
            credentials: "same-origin"
        };
        const response = await fetch(FETCH_BASKET, fetchInit);
        return await response.json();
    },

};

export default Api;