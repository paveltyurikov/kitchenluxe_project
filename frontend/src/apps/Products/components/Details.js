import React from 'react';
import ProductImage from './Image'

const ProductDetails = ({closeProduct, title, images, description}) => {
    return (
        <div className={'product-details'}>

            <div className={'product-details-body'}>
                <span onClick={() => closeProduct()} className="control">close</span>
                <div className={'product-details-body-content container'}>

                    <div className={'product-details-body-content-title'}>{title}</div>
                    {
                        images.length > 0 &&
                        images.map(image => <ProductImage key={image.id} {...image}/>)
                    }
                    <div className={'product-details-body-content-description'}>{description}</div>
                </div>

            </div>
        </div>)
};

export default ProductDetails;