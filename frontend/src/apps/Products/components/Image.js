import React from 'react';

const ProductImage = ({product_list_253_380, product_list_320_480}) => {
    return (
        <div className={'product-image'}
        style={{
            backgroundImage: `url(${product_list_320_480})`
        }}/>
    )
};

export default ProductImage;