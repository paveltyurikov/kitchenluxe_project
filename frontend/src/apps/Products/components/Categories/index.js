import React from 'react';
import './index.css'

const Category = ({category, onClick, className, children}) => (
    <div className={`category-${className ? className : 'root'}`}>
        <div id={category.id} onClick={onClick}
             className={`category-${className ? className : 'root'}-title`}>{category.name}</div>
        {children ? children : null}
    </div>
);

const Categories = ({categories, onCategoryClick}) => {
    return (
        <div className="categories">
            {
                categories.length &&
                categories.map(category => (

                    <Category onClick={onCategoryClick} key={category.id} className="root" category={category}>
                        {category.subCategories && category.subCategories.length &&
                        category.subCategories.map(subCategory => (
                            <Category key={subCategory.id} className="child" category={subCategory}/>
                        ))
                        }
                    </Category>

                ))}
        </div>
    )
}

export default Categories;