import React from 'react';

const ProductOptionChoiceInput = (props) => {
    return (
        <label>
            <input
                className={props.name}
                type={'radio'}
                name={props.name}
                value={props.id}
                onChange={props.onChange}
                placeholder={props.placeholder}/>
            {props.label}
        </label>
    )
};

export default ProductOptionChoiceInput;