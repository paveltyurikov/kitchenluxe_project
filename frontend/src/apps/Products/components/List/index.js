import React from 'react';
import Product from '../Product/index';
import Button from 'components/Button'
import './index.css';

const ProductsList = ({products, selectProduct, fetchProductDetails}) => (
    <div className="products">
        {
            products.map(product => (
                    <Product
                        selectProduct={selectProduct}
                        fetchProductDetails={fetchProductDetails}
                        key={product.id}
                        {...product}/>
                )
            )
        }
    </div>
);

export default ProductsList;

