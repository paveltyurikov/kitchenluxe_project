import React from 'react';
import ProductOptionInput from './OptionInput';
import InputRadio from '../../components/InputRadio';

const ProductOption = (props) => {
    if (props.choices && props.choices.length > 0) {
        return (
            <div>
                <div>{props.name}</div>
                {props.choices.map((choice, index) => {
                    return (
                        <InputRadio
                            key={choice.id}
                            index={index}
                            id={`id_${choice.id}`}
                            name={props.code}
                            value={choice.choice}
                            checked={props.checked}
                            label={choice.choice}
                            onChange={props.onChange}
                            disabled={props.disabled}
                            additionalText={'no add text'}/>
                    )
                })}
            </div>
        )
    }
    return <ProductOptionInput
        onChange={props.onChange}
        label={null}
        type={'text'}
        name={props.code}
        value={props.value}
        placeholder={props.name}
    />;
};

export default ProductOption;