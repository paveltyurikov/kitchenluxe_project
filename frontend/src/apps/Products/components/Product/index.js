import React, {Component} from 'react';
import ImageDiv from 'components/ImageDiv';
import ImageLoadable from 'components/ImageLoadable';
import Button from 'components/Button';
import {Link} from 'react-router-dom';

import './index.css';

class Product extends Component {
    orderClicked = () => {
        const {id, selectProduct} = this.props;
        selectProduct(id)
    };

    render() {
        const {
            title,
            url,
            categories,
            price,
            images,
            selectProduct,
            selectedProduct,
            fetchProductDetails,
            id
        } = this.props;
        return (
            <div id={`product-${id}`} className={'product'}>
                <div className="product-image-wrap">
                    {
                        images.length > 0 &&
                        <ImageLoadable
                            className="product-image"
                            src={images[0].product_list_320_480}
                        />
                    }
                </div>
                <div className="product-category">
                    {categories[0]}
                </div>
                <div className={'product-title'}>
                    {title}
                </div>
                <div className="product-price">
                    <span className="price">${price.excl_tax}</span>&nbsp;
                    {/*<span className="currency">{price.currency}</span>*/}
                </div>
                <div className="product-controls">
                    <Button
                        className="btn-order"
                        onClick={this.orderClicked}>
                        Order
                    </Button>
                </div>
            </div>)
    }
}

export default Product;