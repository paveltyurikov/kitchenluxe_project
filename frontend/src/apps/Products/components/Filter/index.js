import React, {Component} from 'react';
import Categories from '../Categories'
import Hamburger from 'components/Hamburger'
import './index.css'


class ProductsFilter extends Component {
    state = {
        opened: false
    };
    onCategoryClick = (url) => {
        this.props.fetchProducts(url)
    };
    hamburgerClick = () => {
        this.setState({opened: !this.state.opened});
    };

    render() {
        const {categories, title} = this.props;
        const {opened} = this.state;
        return (
            <div className="products-filter">

                <div className="products-filter-bar">
                    <Hamburger
                        opened={opened}
                        onClick={this.hamburgerClick}
                    />
                    {title && !opened && <div className="products-filter-bar-title">{title}</div>}
                </div>
                <div className={`products-filter-wrap ${opened ? 'opened' : ''}`}>
                    <div className="products-filter-wrap-content">
                        {title && <h1>{title}</h1>}
                        <Categories
                            onCategoryClick={this.onCategoryClick}
                            categories={categories}/>
                    </div>
                </div>

            </div>
        )
    }
}

export default ProductsFilter;