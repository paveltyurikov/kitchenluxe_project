import React from 'react';

const ProductOptionInput = (props) => {
    return (
        <div className={'option-input'}>
            <label>{props.label}</label>
            <input
                className={props.name}
                type={props.type}
                name={props.name}
                value={props.value}
                onChange={props.onChange}
                placeholder={props.placeholder}/>

        </div>
    )
};

export default ProductOptionInput;