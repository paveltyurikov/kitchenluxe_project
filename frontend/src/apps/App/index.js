import React, {Component} from 'react';

import {ConnectedRouter} from 'react-router-redux';

import Layout from 'components/Layout/index'
import {BasketWrapper} from '../Basket'
import {Route, Switch} from "react-router-dom";
import {withAuthentication} from "../Auth";
import NotFound from "../../components/NotFound";
import {AuthView, BasketView, CheckOut, HomePage, ProductsView} from "routes";


export default class App extends Component {

    render() {
        const {history} = this.props;
        return (
            <ConnectedRouter history={history}>
                <Layout>

                    <Switch>
                        <Route exact path="/" component={HomePage}/>
                        <Route path="/auth" component={withAuthentication(AuthView)}/>
                        <Route exact path="/cart" render={(match) =>
                            <BasketWrapper>
                                {
                                    ({...basketProps}) => <BasketView {...match}
                                                                      {...basketProps}
                                    />

                                }
                            </BasketWrapper>
                        }/>
                        <Route exact path="/checkout" render={(match) =>
                            <BasketWrapper>
                                {
                                    ({...basketProps}) => <CheckOut {...match}
                                                                    basket={basketProps.basket}
                                                                    basketLines={basketProps.basketLines}
                                    />

                                }
                            </BasketWrapper>
                        }/>
                        {/*<Route path="/checkout/success" render={() => <CheckOutSuccess basket={basket}/>}/>*/}
                        <Route exact path="/products/:series?"
                               render={(match) =>
                                   <BasketWrapper>
                                       {
                                           ({...basketProps}) => <ProductsView {...match}
                                                                               basket={basketProps.basket}
                                                                               basketLines={basketProps.basketLines}/>

                                       }
                                   </BasketWrapper>
                               }
                        />
                        <Route component={NotFound}/>
                    </Switch>
                </Layout>
            </ConnectedRouter>
        )
    }
}

