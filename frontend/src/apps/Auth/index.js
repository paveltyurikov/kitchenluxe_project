import React, {Component} from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from "redux";
import * as actions from './actions'
import Login from './components/Login'
import './index.css'

export const withAuthentication = (Component) => {
    const ModifiedComponent = ({isAuthenticated, authLogin, authLogout, error, match}) => {
        return isAuthenticated ?
            (<Component match={match} authLogout={authLogout}/>)
            //("hello")
            :
            (<Login error={error} authLogin={authLogin}/>);
    };

    const mapStateToProps = state => (state.session);
    const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

    return connect(
        mapStateToProps,
        mapDispatchToProps)(ModifiedComponent);
};

const mapStateToProps = state => (state.session);
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

class AuthView extends Component {
    render(){
        return(
            <div className="auth">
                <h1>Auth view</h1>
            </div>
        )
    }
}

export default connect(
        mapStateToProps,
        mapDispatchToProps)(AuthView);