import * as actionTypes from './actiontypes';
import {getSessionId} from 'utils';

const authInitialState =
    {
        dataIsFetching: false,
        some: false,
        isAuthenticated: getSessionId(),
        errors: {},
        sessionId: getSessionId()
    };

export default function sessionReducer(state = authInitialState, action) {

    switch (action.type) {

        case actionTypes.DATA_IS_FETCHING: {
            return {
                ...state,
                dataIsFetching: action.bool,
            };
        }
        case actionTypes.LOGIN: {
            return {
                ...state,
                dataIsFetching: true,
                error: ''
            };
        }
        case actionTypes.LOGIN_SUCCESS: {
            return {
                ...state,
                isAuthenticated: true,
                dataIsFetching: false,
            };
        }
        case actionTypes.LOGIN_FAILED: {
            const error = JSON.parse(action.data) ? JSON.parse(action.data).error : 'Ошибка';
            return {
                ...state,
                isAuthenticated: false,
                dataIsFetching: false,
                error: error,
            };
        }
        case actionTypes.LOGOUT: {
            return {
                ...state,
                dataIsFetching: true,
            };
        }
        case actionTypes.LOGOUT_SUCCESS: {
            return {
                ...state,
                isAuthenticated: false,
                dataIsFetching: true,
            };
        }

        default:
            return state;
    }
}
