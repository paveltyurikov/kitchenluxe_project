import React from 'react'

const FormInput = ({
                       label,
                       placeholder,
                       type,
                       value,
                       name,
                       className,
                       id,
                       onChange,
                       onBlur,
                       error,
                       touched,
                       disabled
                   }) => (
    <div className={`form-input ${className ? className : ''}`}>
        {label && <label>{label}</label>}
        <input
            id={id}
            name={name}
            placeholder={placeholder}
            disabled={disabled}
            type={type}
            value={value}
            onChange={onChange}
            onBlur={onBlur}
            className={error ? `text-input error` : 'text-input'}
        />
    </div>
);

export default FormInput;