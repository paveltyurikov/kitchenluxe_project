import React, {Component} from 'react';
import Button from 'components/Button'
import FormInput from './FormInput'

class Login extends Component {
    state = {
        login: {
            username: '',
            email: '',
            phone_number: '',
            password: '',
        },
        loginWith: 'username'
    };
    changeLoginWith = (to) => {
        this.setState({loginWith: to})
    };

    handleChange = (e) => {
        const login = {...this.state.login};
        login[e.target.name] = e.target.value;
        this.setState({login: login})
    };
    checkActive = (loginWith) => {

    };

    handleSubmit = (e) => {
        e.preventDefault();
        this.props.authLogin(this.state.login)
    };

    render() {
        const {username, email, password} = this.state;
        const {error} = this.props;
        return (
            <div className={'login-view'}>
                <form onSubmit={this.handleSubmit} className={'login-form centered-block'}>
                    <div className="login-user-select">
                        <Button
                            className={this.state.loginWith === 'username' ? 'active' : null}
                            onClick={() => this.changeLoginWith('username')}>Username</Button>
                        <Button
                            className={this.state.loginWith === 'email' ? 'active' : null}
                            onClick={() => this.changeLoginWith('email')}>Email</Button>
                        <Button
                            className={this.state.loginWith === 'phone_number' ? 'active' : null}
                            onClick={() => this.changeLoginWith('phone_number')}>Phone Number</Button>
                    </div>
                    <FormInput
                        label={this.state.loginWith}
                        type="text"
                        name={this.state.loginWith}
                        value={this.state[this.state.loginWith]}
                        onChange={this.handleChange}
                    />
                    <FormInput
                        label="Password"
                        type={'password'}
                        name={'password'}
                        value={password}
                        onChange={this.handleChange}
                    />
                    {error && <div className="login-form-error">{error}</div>}
                    <div className="form-input">
                        <Button type="submit" className="login-form-btn">Login</Button>
                    </div>
                </form>
            </div>
        )
    }

}

export default Login;