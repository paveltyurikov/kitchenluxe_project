import React, {Component} from 'react';
import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import reducer from './reducer';
import saga from './saga';
import * as actions from "./actions";
import FormikForm from "components/FormikForm";
import {fields, formValidationSchema} from "./components/Form/fields";
import Button from 'components/Button'
import BasketLine from 'components/Basket/Line'

class CheckoutProxy extends Component {
    state = {
        basket: null,
        guest_email: 'zombie@have.no',
        total: null,
        shipping_method_code: "no-shipping-required",
        shipping_address: {
        }
    }

    componentDidMount() {
        const {fetchShipping} = this.props;
        fetchShipping()
    }

    componentWillReceiveProps(newProps) {
        const {basket} = newProps;
        if (this.state.basket === null) {
            if (basket !== null && Object.keys(basket).length) {
                this.setState(
                    {
                        basket: basket.url,
                        total: basket.total_incl_tax,
                    }
                )
            }
        }

    }

    formatSippingMethod = (shippingMethod) => {
        return Object.keys(shippingMethod).length ? `${shippingMethod.name} - ${shippingMethod.price.currency === 'USD' ? '$' : ''}${shippingMethod.price.incl_tax}` : ''
    }

    placeOrder = (values) => {
        const {placeOrder} = this.props
        const data = {...this.state}
        data.shipping_address = values
        placeOrder(data)
    }

    formSubmit

    render() {
        const {shippingMethods, basketLines, requestErrors, clearRequestErrors, checkoutData} = this.props;
        return Object.keys(checkoutData).length?
            (
                <div>{JSON.stringify(checkoutData, 4)}
                    <div>Order: #{checkoutData.number}</div>
                </div>
            )
            :(
            <div className="checkout">
                <h1 className="text-center">Checkout</h1>
                <h2>Order content</h2>
                <div>
                    {basketLines.length &&
                    basketLines.map(line => <BasketLine key={line.url} {...line}/>)}
                </div>
                <h2>Shipping</h2>

                {
                    shippingMethods.length &&
                    <div className="shipping-methods">
                        {
                            shippingMethods.map(shippingMethod => (
                                    <div key={shippingMethod.code} className="shipping-methods-item">
                                        <div
                                            className="shipping-methods-title">{this.formatSippingMethod(shippingMethod)}</div>
                                    </div>
                                )
                            )
                        }
                    </div>
                }
                <FormikForm
                    className="m-quiz-round-form"
                    fields={fields}
                    formSubmit={this.placeOrder}
                    requestErrors={requestErrors}
                    clearRequestErrors={clearRequestErrors}
                    formValidationSchema={formValidationSchema}
                    displayName="Login Form"
                    formSubmitButtonText={"Place Order"}
                />
            </div>
        )
    }
}


const mapStateToProps = state => (state.checkout);
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);
const withReducer = injectReducer({key: 'checkout', reducer});
const withSaga = injectSaga({key: 'checkout', saga});
const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect)(CheckoutProxy);


