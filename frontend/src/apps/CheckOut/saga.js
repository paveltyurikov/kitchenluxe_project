import {call, put, takeLatest} from 'redux-saga/effects'
import Api from './api'
import * as checkoutActionTypes from './actiontypes'
import * as checkoutActions from './actions'


export function* fetchOrderLines(action) {
    try {
        const data = yield call(Api.fetchOrderLines, action);
        yield put(checkoutActions.fetchOrderLinesSuccess(data));
    } catch (error) {
        console.log(error.message);
        yield put(checkoutActions.dataIsFetching(false))
    }
}

export function* fetchShipping(action) {
    try {
        const data = yield call(Api.fetchShipping, action);
        yield put(checkoutActions.fetchShippingSuccess(data));
    } catch (error) {
        console.log(error.message);
        yield put(checkoutActions.dataIsFetching(false))
    }
}
export function* placeOrder(action) {
    try {
        const data = yield call(Api.placeOrder, action);
        yield put(checkoutActions.placeOrderSuccess(data));
    } catch (error) {
        console.log(error.message);
        yield put(checkoutActions.dataIsFetching(false))
    }
}



export default function* basketSaga() {
    yield takeLatest(checkoutActionTypes.FETCH_ORDER_LINES, fetchOrderLines);
    yield takeLatest(checkoutActionTypes.FETCH_SHIPPING, fetchShipping);
    yield takeLatest(checkoutActionTypes.PLACE_ORDER, placeOrder);
}

