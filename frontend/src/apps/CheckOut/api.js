
import {getHeaders} from 'utils/index'
import request from 'utils/request'

export const SHIPPING = '/api/basket/shipping-methods/';
export const CHECKOUT = '/api/checkout/';


const Api = {
    fetchShipping: async (action) => {
        return request(
            {
                url: SHIPPING,
            }
        );
    },
    placeOrder: async (action) => {
        return request(
            {
                url: CHECKOUT,
                options:{
                    method: 'POST',
                    headers: getHeaders(),
                    body: JSON.stringify(action.data)
                }
            }
        );
    },
};

export default Api;