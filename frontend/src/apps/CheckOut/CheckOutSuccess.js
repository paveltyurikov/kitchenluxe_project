import React, {Component} from 'react'
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as checkoutAction from "./actions";
import './index.css'
const d = {
    number: "100091",
    basket: "http://localhost:9006/api/baskets/91/",
    billing_address: null,
    currency: "USD",
    date_placed: "2018-02-23T11:00:22.334093+03:00",
    guest_email: "dom116@gmail.com",
    lines: "http://localhost:9006/api/orders/14/lines/",
    offer_discounts: [],
    owner: null,
    payment_url: "You need to implement a view named 'api-payment' which redirects to the payment provider and sets up the callbacks.",
    shipping_address:
        {
            country: "http://localhost:9006/api/countries/US/",
            first_name: "",
            id: 17,
            last_name: "",
            line1: "No address",
            line2: "",
            line3: "",
            line4: "",
            notes: "",
            phone_number: "+14077664401",
            postcode: "",
            search_text: "No address United States of America",
            state: "",
            title: "",
            shipping_code: "no-shipping-required",
            shipping_excl_tax: "0.00",
            shipping_incl_tax: "0.00",
            shipping_method: "Доставка не требуется",
            status: "new",
            total_excl_tax: "24.00",
            total_incl_tax: "24.00",
            url: "http://localhost:9006/api/orders/14/",
            voucher_discounts: [],
        }
}

class CheckoutSuccess extends Component {
    componentDidMount() {
        const {fetchOrderLines, checkoutData} = this.props;
        Object.keys(checkoutData).length > 0 && fetchOrderLines(checkoutData.lines)
    }

    render() {

        const {number, guest_email, email, phone_number} = this.props.checkoutData;
        return (
            <div className={'checkout-success'}>
                <h1>Checkout Success</h1>
                <div className={'order'}>
                    <div className={'order-number'}>#{number}</div>
                    <div className={'order-email'}>{guest_email || email}</div>
                    <div className={'order-phone'}>#{phone_number}</div>
                </div>
            </div>
        )
    }
}

//export default CheckoutSuccess;
const mapStateToProps = state => (state.checkout);

const mapDispatchToProps = dispatch => bindActionCreators(checkoutAction, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CheckoutSuccess);
