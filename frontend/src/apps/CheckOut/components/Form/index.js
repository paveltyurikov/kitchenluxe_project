import React, {Component} from 'react';
import FormikForm from "components/FormikForm";
import {fields, formValidationSchema} from "./fields";


class CheckOutForm extends Component {
    formSubmit = (values, {...other}) => {
        const {getFormData} = this.props;
        getFormData(values)
    };
    render() {

        const {requestErrors, clearRequestErrors, formSubmitText, edit, formInitialValues} = this.props;

        return (
            <FormikForm
                className="chekout-form"
                fields={fields}
                formSubmit={this.formSubmit}
                formInitialValues={formInitialValues}
                requestErrors={requestErrors}
                clearRequestErrors={clearRequestErrors}
                formValidationSchema={formValidationSchema}
                displayName="Login Form"
                formSubmitButtonText={formSubmitText ? formSubmitText : "Place Order"}
            />
        )
    }
}


export default CheckOutForm;