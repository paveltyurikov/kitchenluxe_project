import {object, string} from "yup";

export const fields = [
    {
        name: "country",
        fieldType: 'input',
        type:'hidden',
        defaultValue: "http://localhost:9006/api/countries/US/"
    },
    {
        name: "phone_number",
        fieldType: 'input',
        placeholder: 'Phone',
    },
    {
        name: "first_name",
        fieldType: 'input',
        placeholder: 'name',
    },
    {
        name: "last_name",
        fieldType: 'input',
        placeholder: 'Last Name',
    },
    {
        name: "line1",
        fieldType: 'input',
        placeholder: 'Street, Bld',
    },
    {
        name: "line4",
        fieldType: 'input',
        placeholder: 'City',
    },
];

export const formValidationSchema = object().shape({
    phone_number: string()
        .required('Fill this field please'),
    first_name: string()
        .required('Fill this field please'),
});