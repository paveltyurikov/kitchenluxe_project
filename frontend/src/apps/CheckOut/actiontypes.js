export const DATA_IS_FETCHING = 'basket/DATA_IS_FETCHING';

export const PLACE_ORDER = 'basket/PLACE_ORDER';
export const PLACE_ORDER_SUCCESS = 'basket/PLACE_ORDER_SUCCESS';


export const FETCH_ORDER_LINES = 'checkout/FETCH_ORDER_LINES';
export const FETCH_ORDER_LINES_SUCCESS = 'checkout/FETCH_ORDER_LINES_SUCCESS';

export const FETCH_SHIPPING = 'checkout/FETCH_SHIPPING';
export const FETCH_SHIPPING_SUCCESS = 'checkout/FETCH_SHIPPING_SUCCESS';