import * as checkOutActionTypes from "./actiontypes";

import {makeActionCreator} from 'api/index'

export const dataIsFetching = makeActionCreator(checkOutActionTypes.DATA_IS_FETCHING, 'bool');
export const placeOrder = makeActionCreator(checkOutActionTypes.PLACE_ORDER, 'data');
export const placeOrderSuccess = makeActionCreator(checkOutActionTypes.PLACE_ORDER_SUCCESS, 'data');

export const fetchOrderLines = makeActionCreator(checkOutActionTypes.FETCH_ORDER_LINES, 'url');
export const fetchOrderLinesSuccess = makeActionCreator(checkOutActionTypes.FETCH_ORDER_LINES_SUCCESS, 'data');

export const fetchShipping = makeActionCreator(checkOutActionTypes.FETCH_SHIPPING, 'url');
export const fetchShippingSuccess = makeActionCreator(checkOutActionTypes.FETCH_SHIPPING_SUCCESS, 'data');
