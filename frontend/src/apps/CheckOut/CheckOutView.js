import React from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {withFormik} from 'formik';
import {string} from 'yup';
import * as checkoutAction from './actions'
import CheckOutFormTemplate from '../../components/Checkout/CheckOutFormTemplate'
import './index.css'

const propsToValues = {
    first_name: '',
    last_name: '',
    phone_number: '',
    guest_email: '',
    line1: '',
    line2: '',
    line3: '',
    line4: '',
    notes: '',
};

const yupShape = {
    first_name: string()
        .required('Ruquired Field'),
    phone_number: string()
        .required('Ruquired Field'),
    guest_email: string()
        .required('Ruquired Field'),
};

const CheckOutForm = withFormik({
    mapPropsToValues: props => (propsToValues),
    // Add a custom validation function (this can be async too!)
    validate: (values, props) => {
        const errors = {};
        Object.keys(values).map(key => {
                if (!values[key]) {
                    errors[key] = `Fill ${key} field, please`;
                }
            }
        )
        return errors;
    },

    displayName: 'CheckOutForm',

})(CheckOutFormTemplate);

//export default CheckOutView;
const mapStateToProps = state => (state.checkout);

const mapDispatchToProps = dispatch => bindActionCreators(checkoutAction, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(CheckOutForm);