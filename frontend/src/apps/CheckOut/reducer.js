import * as checkOutActionTypes from "./actiontypes";
import {createReducer} from 'api/index'

const initialCheckOutState = {
    checkoutData: {},
    shippingMethods:[]
};


const checkOutReducer = createReducer(initialCheckOutState, {

    [checkOutActionTypes.PLACE_ORDER_SUCCESS](state, action) {
        return {
            ...state,
            checkoutData: action.data
        };
    },
    [checkOutActionTypes.FETCH_SHIPPING_SUCCESS](state, action) {
        return {
            ...state,
            shippingMethods: action.data
        };
    },
    [checkOutActionTypes.FETCH_ORDER_LINES_SUCCESS](state, action) {
        return {
            ...state,
            orderLines: action.data
        };
    },
});

export default checkOutReducer;