import * as actionTypes from "./actiontypes";
import {makeActionCreator} from 'api/index'

export const dataIsFetching = makeActionCreator(actionTypes.DATA_IS_FETCHING, 'bool');
export const fetchBasket = makeActionCreator(actionTypes.FETCH_BASKET);
export const fetchBasketSuccess = makeActionCreator(actionTypes.FETCH_BASKET_SUCCESS, 'data');
export const fetchBasketLines = makeActionCreator(actionTypes.FETCH_BASKET_LINES, 'url');
export const fetchBasketLinesSuccess = makeActionCreator(actionTypes.FETCH_BASKET_LINES_SUCCESS, 'data');
export const deleteBasketLine = makeActionCreator(actionTypes.DELETE_BASKET_LINE, 'url');
export const deleteBasketLineSuccess = makeActionCreator(actionTypes.DELETE_BASKET_LINE_SUCCESS, 'url');
export const checkoutBasket = makeActionCreator(actionTypes.CHECKOUT_BASKET, 'data');
export const checkoutBasketSuccess = makeActionCreator(actionTypes.CHECKOUT_BASKET_SUCCESS, 'data');