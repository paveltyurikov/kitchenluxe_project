import {call, put, takeLatest} from 'redux-saga/effects'
import Api from './api'
import * as actionTypes from './actiontypes'
import * as actions from './actions'


export function* fetchBasket(action) {
    try {
        const data = yield call(Api.fetchBasket, action);
        yield put(actions.fetchBasketSuccess(data));
        yield put(actions.fetchBasketLines(data.lines))
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* refreshBasket(action) {
    try {
        const data = yield call(Api.fetchBasket, action);
        yield put(actions.fetchBasketSuccess(data));
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* fetchBasketLines(action) {
    try {
        const data = yield call(Api.fetchBasketLines, action);
        yield put(actions.fetchBasketLinesSuccess(data))
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* deleteBasketLine(action) {
    try {
        const success = yield call(Api.deleteBasketLine, action);
        if (success) {
            yield put(actions.deleteBasketLineSuccess(action.url));
            const data = yield call(Api.fetchBasket, action);
            yield put(actions.fetchBasketSuccess(data));

        }
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}

export function* checkoutBasket(action) {
    try {
        const data = yield call(Api.checkoutBasket, action);
        yield put(actions.checkoutBasketSuccess(data))
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}


export default function* basketSaga() {
    yield takeLatest(actionTypes.FETCH_BASKET, fetchBasket);
    yield takeLatest(actionTypes.CHECKOUT_BASKET, checkoutBasket);
    yield takeLatest(actionTypes.FETCH_BASKET_LINES, fetchBasketLines);
    yield takeLatest(actionTypes.DELETE_BASKET_LINE, deleteBasketLine);
}

