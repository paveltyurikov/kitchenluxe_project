import {getHeaders} from 'api/index'

export const FETCH_BASKET = '/api/basket/';
export const CHECKOUT_BASKET = '/api/checkout/';

const Api = {
    fetchBasket: async () => {
        const fetchInit = {
            method: 'GET',
            headers: getHeaders(),
            credentials: "same-origin"
        };
        const response = await fetch(FETCH_BASKET, fetchInit);
        return await response.json();
    },
    fetchBasketLines: async (action) => {
        const response = await fetch(
            action.url.replace(/^https?:/, ''),
            {
                method: 'GET',
                headers: getHeaders(),
                credentials: "same-origin",
            });

        return await response.json();
    },
    deleteBasketLine: async (action) => {
        const response = await fetch(
            action.url.replace(/^https?:/, ''),
            {
                method: 'DELETE',
                headers: getHeaders(),
                credentials: "same-origin",
            });
        return response.ok
    },
    checkoutBasket: async (action) => {
        const fetchInit = {
            method: 'POST',
            headers: getHeaders(),
            credentials: "same-origin",
            body: JSON.stringify(action.data)
        };
        const response = await fetch(
            CHECKOUT_BASKET,
            fetchInit);
        return await response.json();
    },

};

export default Api;