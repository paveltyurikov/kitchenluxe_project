import React, {Component} from 'react';

import ProductBasketLine from './BasketLine'

const BasketProductLines = ({
                                basketLines,
                                deleteBasketLine,
                                editBasketLine,
                                selectedProduct
                            }) => {
    const lines = basketLines && basketLines.length > 0 ?
        basketLines.filter(line => line.product.id === selectedProduct.id)
        :
        basketLines
    ;

    return (
        <div className="basket">
            {
                lines && lines.length > 0 &&
                lines.map((line, index) => (
                        <ProductBasketLine
                            key={line.url}
                            deleteBasketLine={deleteBasketLine}
                            editBasketLine={editBasketLine}
                            {...line}
                        />
                    )
                )
            }

        </div>
    )
};

export default BasketProductLines;
