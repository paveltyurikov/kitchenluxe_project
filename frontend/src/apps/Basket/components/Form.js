import React, {Component} from "react";


import ProductBasketLines from './BasketLines'
import Button from 'components/Button'
import FormField from './FormField'




const checkFiled = (field, errors, touched) => {
    return errors[field] && touched[field]
};


class Form extends Component {
    constructor(props) {
        super(props);
        const {url, options} = this.props.selectedProduct;
        const edgeChoices = options.filter(option => option.code === 'edge')[0].choices;
        this.state = {
            url: url,
            selectedChoice: 0,
            options: options,
            edgeChoices: edgeChoices
        };

    }

    componentDidMount() {
        this.props.setTouched('edge', true);
        this.props.setFieldValue('edge', this.state.edgeChoices[0].choice);
    }

    setChoice(choice, index) {
        this.props.setFieldValue('edge', choice.choice);
        this.setState({selectedChoice: index});
    }


    onSubmit = (e) => {
        e.preventDefault();
        const {addProductToBasket, setTouched, values, isValid} = this.props;
        if (isValid) {
            const {options, url} = this.state;
            const {quantity} = values;
            let newOptions = [];
            Object.keys(options).map(key => {
                    if (options[key].code !== 'quantity') {
                        newOptions.push(
                            {
                                option: options[key].url,
                                value: values[options[key].code]
                            }
                        )
                    }
                }
            );
            addProductToBasket(url, quantity, newOptions)
        }

    }

    render() {
        const {selectedChoice, edgeChoices, images} = this.state;
        const {
            closeProduct,
            selectedProduct,
            basketLines,
            basket,
            deleteBasketLine,
            dirty,
            values,
            touched,
            errors,
            handleChange,
            handleBlur
        } = this.props;
        const renderChoice = (choice, index) => (
            <Button
                key={choice.id}
                onClick={() => this.setChoice(choice, index)}
                className={selectedChoice === index ? 'active' : ''}>
                {choice.choice}
            </Button>
        );

        return (
            <div className="add-product-form">
                <Button
                    className="add-product-form-bg"
                    onClick={() => {
                        closeProduct()
                    }}/>
                <div className="add-product-form-body">
                    <ProductBasketLines
                        basketLines={basketLines}
                        deleteBasketLine={deleteBasketLine}
                        selectedProduct={selectedProduct}
                    />
                    {
                        basket !== null && basketLines.length > 0 &&
                        <div className="basket-total">Total <b>{basket.total_incl_tax}</b></div>
                    }
                    <div className={'add-product-form-title'}>{selectedProduct.title}</div>
                    <form  onSubmit={this.onSubmit}>
                        <FormField
                            className="switch"
                            label="Edgebanding"
                        >
                            <div className={'add-product-form-field-errors'}>
                                {errors && checkFiled('edge', touched, errors) && errors.edge}</div>
                            <div className="add-product-form-field-inputs">
                                {edgeChoices.map((choice, index) => renderChoice(choice, index))}
                            </div>
                        </FormField>


                        <FormField
                            className="dimension"
                            label="Dimension"
                        >
                            <div
                                className={'add-product-form-field-errors'}>
                                {errors && checkFiled('width', touched, errors) &&
                                errors.width}
                                {errors && checkFiled('height', touched, errors) &&
                                errors.height}</div>
                            <div className="add-product-form-field-inputs">
                                <input
                                    name={'width'}
                                    type={'number'}
                                    value={values.width}
                                    placeholder={'Width'}
                                    onBlur={handleBlur}
                                    onChange={handleChange}/>
                                <input
                                    name={'height'}
                                    type={'number'}
                                    value={values.height}
                                    placeholder={'Height'}
                                    onBlur={handleBlur}
                                    onChange={handleChange}/>
                            </div>
                        </FormField>
                        <FormField
                            className="dimension"
                            label="Quantity"
                        >
                            <div
                                className={'add-product-form-field-errors'}>
                                {errors && checkFiled('quantity', touched, errors) &&
                                errors.quantity}</div>
                            <div className="add-product-form-field-inputs-group">
                                <input
                                    id="add-product-form-quantity-input"
                                    type={'number'}
                                    name={'quantity'}
                                    onBlur={handleBlur}
                                    onChange={handleChange}
                                    value={values.quantity}
                                    maxLength={3}
                                />
                                <Button type={'submit'}>
                                    Add
                                </Button>
                            </div>
                        </FormField>
                        <Button
                            onClick={(e) => {
                                e.preventDefault();
                                closeProduct()
                            }}>
                            Close
                        </Button>
                    </form>
                </div>
            </div>
        )
    }
};
export default Form;

