import React from 'react'

const FormField = ({className, label, children, errors}) => (
    <div className={`add-product-form-field ${className ? className : ''}`}>
        {
            label &&
            <label>{label}</label>
        }
        {children}
    </div>
);

export default FormField;