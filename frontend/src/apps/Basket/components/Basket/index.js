import React, {Component} from 'react'
import {Link} from "react-router-dom";

import BasketMiniView from "components/Basket/MiniView";
import BasketEmpty from "components/Basket/Empty";

export default class Basket extends Component {
    render() {
        const {basketLines, deleteBasketLine, editBasketLine, basket} = this.props;
        return basketLines.length ?
            (
                <div className="basket-view">
                    <BasketMiniView

                        lines={basketLines}
                        deleteBasketLine={deleteBasketLine}
                        editBasketLine={editBasketLine}/>
                    {
                        basket !== null && basketLines.length > 0 &&
                        <div

                            className="text-right">Total <b>{basket.total_total} USD</b></div>
                    }
                    {
                        basket !== null && basketLines.length > 0 &&
                        <div className="text-center"><Link className={'btn'} to={'/checkout'}>Checkout</Link></div>
                    }

                </div>
            )
            :
            (
                <BasketEmpty/>
            );
    }
}