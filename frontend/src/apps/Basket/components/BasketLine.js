import React from 'react';

const ProductBasketLine = ({
                               index,
                               url,
                               date_created,
                               product,
                               deleteBasketLine,
                               editBasketLine,
                               attributes,
                               edge,
                               width,
                               height,
                               quantity,
                               price_incl_tax,
                               line_total_price_incl_tax,
                               price_currency
                           }) => {
    return (
        <div
            className="product-basket-line">
            <div className={'controls'}>
            <span
                onClick={() => deleteBasketLine(url, index)}
                className={'control gold'}>remove</span>
                {/*<span*/}
                {/*onClick={() => editBasketLine(index)}*/}
                {/*className={'control'}>edit</span>*/}
            </div>
            <div className="product-basket-line-title">
                {product.title}
            </div>
            <div className="product-basket-line-attrs">
                {edge}
            </div>
            <div className="product-basket-line-attrs">
                <span className={'text-light'}>(W x H x Quantity)</span> <b>{width}</b> x <b>{height}</b> <span
                className={'text-light'}>{`x ${quantity}`}</span>
            </div>
            <div className="product-basket-line-attrs text-right">
                {line_total_price_incl_tax} {price_currency}
            </div>

        </div>)
};

export default ProductBasketLine;