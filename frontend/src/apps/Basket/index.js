import React, {Component} from 'react';

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import reducer from './reducer';
import saga from './saga';
import Basket from './components/Basket'

import * as actions from "./actions";
import './index.css'


class BasketProvider extends Component {
    componentDidMount() {
        this.props.fetchBasket()
    }
    render() {
        return this.props.children(
            {
                ...this.props
            }
        )
    }
}

class BasketView extends Component {
    render() {
        return <Basket {...this.props}/>
    }
}



const mapStateToProps = state => (state.basket);
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const withReducer = injectReducer({key: 'basket', reducer});
const withSaga = injectSaga({key: 'basket', saga});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export const BasketWrapper =  compose(
    withReducer,
    withSaga,
    withConnect)(BasketProvider);

export default compose(
    withConnect)(BasketView);
