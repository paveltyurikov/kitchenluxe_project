import * as actionTypes from './actiontypes';
import {createReducer} from 'api/index'

const basketInitialState = {
    basket: null,
    basketLines: [],
};

const basketReducer = createReducer(basketInitialState, {

    [actionTypes.DATA_IS_FETCHING](state, action) {
        return {
            ...state,
            dataIsFetching: action.bool
        };
    },
    [actionTypes.FETCH_BASKET_SUCCESS](state, action) {
        return {
            ...state,
            basket: action.data,
        };
    },
    [actionTypes.FETCH_BASKET_LINES_SUCCESS](state, action) {
        if (action.data && action.data.length > 0) {
            return {
                ...state,
                basketLines: action.data.reverse()
            };
        }
        return {
            ...state,
            basketLines: action.data
        };
    },

    [actionTypes.DELETE_BASKET_LINE_SUCCESS](state, action) {
        const basketLines = state.basketLines.filter(basketLine => (basketLine.url !== action.url));
        return {
            ...state,
            basketLines: basketLines,
        }
    },

});

export default basketReducer;
