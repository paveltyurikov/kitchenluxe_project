import {call, put, takeLatest} from 'redux-saga/effects'
import Api from './api'
import * as actionTypes from './actiontypes'
import * as actions from './actions'


export function* callMeBack(action) {
    try {
        const data = yield call(Api.callMeBack, action);
        yield put(actions.callMeBackSuccess(data));
    } catch (error) {
        console.log(error.message);
        yield put(actions.dataIsFetching(false))
    }
}



export default function* basketSaga() {
    yield takeLatest(actionTypes.CALLMEBACK, callMeBack);
}

