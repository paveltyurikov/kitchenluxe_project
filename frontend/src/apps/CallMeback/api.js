import {getHeaders} from 'api/index'

export const CALL_ME_BACK = '/api/callmeback/';

const Api = {
    callMeBack: async (action) => {
        const response = await fetch(
            CALL_ME_BACK,
            {
                method: 'POST',
                headers: getHeaders(),
                credentials: "same-origin",
                body: JSON.stringify(action.data)
            });
        return await response.json();
    },

};

export default Api;