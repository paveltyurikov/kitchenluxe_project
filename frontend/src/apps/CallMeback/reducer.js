import * as callMeActionTypes from "./actiontypes";
import {createReducer} from 'api/index'

const initialCheckOutState = {
    dataIsFetching: false,
    callMeBackRequested: false,
    callMeBackData:{},
    callMeBackSuccess: false,
};


const checkOutReducer = createReducer(initialCheckOutState, {

    [callMeActionTypes.DATA_IS_FETCHING](state, action) {
        return {
            ...state,
            dataIsFetching: action.bool
        };
    },

    [callMeActionTypes.CALLMEBACK](state, action) {
        return {
            ...state,
            dataIsFetching: true,
            callMeBackRequested: true,
            callMeBackData: action.data
        };
    },
    [callMeActionTypes.FETCH_ORDER_LINES_SUCCESS](state, action) {
        return {
            ...state,
            dataIsFetching: true,
            callMeBackRequested: true,
            callMeBackSuccess: true,
            callMeBackData: action.data
        };
    },
});

export default checkOutReducer;