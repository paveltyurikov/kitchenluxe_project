import React from 'react';
import FormInput from 'components/Form/Input'

const Form = ({
                  handleSubmit,
                  handleReset,
                  handleChange,
                  handleBlur,
                  values,
                  touched,
                  errors,
              }) => (
    <form>
        <Input
            id="name"
            placeholder="Имя"
            type="text"
            value={values.name}
            onChange={handleChange}
            onBlur={handleBlur}
            touched={touched.name}
            error={errors.name}
        />
    </form>
)