import React, {Component} from 'react';

import Input from 'components/Form/Input'
import Select from 'components/Form/Select'
import InputMasked from 'components/Form/InputMasked'q
import Button from 'components/Button'


class Form extends Component {

    render() {
        const {
            requestErrors,
            handleSubmit,
            handleReset,
            handleChange,
            handleBlur,
            values,
            touched,
            errors,
        } = this.props;
        return (
            <div className={'form-container'}>
                <h1>Test</h1>
                <form id='registration-form'
                      className={'form-bg '}
                      onSubmit={handleSubmit}
                      onReset={handleReset}>

                    <Input
                        id="name"
                        placeholder="Имя"
                        type="text"
                        value={values.name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.name}
                        error={errors.name}
                    />
                    <Input
                        id="second_name"
                        placeholder="Фамилия"
                        type="text"
                        value={values.second_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.second_name}
                        error={errors.second_name}
                    />
                    <Input
                        id="third_name"
                        placeholder="Отчество"
                        type="text"
                        value={values.third_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.third_name}
                        error={errors.third_name}
                    />
                    <Input
                        id="company"
                        placeholder="Компания"
                        type="text"
                        value={values.company}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.company}
                        error={errors.company}
                    />
                    <Input
                        id="position"
                        placeholder="Должность"
                        type="text"
                        value={values.position}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.position}
                        error={errors.position}
                    />

                    <Select
                        id="city"
                        placeholder="Должность"
                        label={'Город'}
                        options={
                            [
                                '',
                                'Москва',
                                'Санкт-Петербург',
                                'Екатеринбург',
                                'Краснодар',
                                'Новосибирск', 'Казань'
                            ]
                        }
                        value={values.city}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.city}
                        error={errors.city}
                    />

                    <Input
                        id="email"
                        placeholder="Email"
                        type="email"
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.email}
                        error={errors.email}
                    />

                    <InputMasked
                        mask="+7 (999) 999-99-99"
                        className={'text-input'}
                        maskChar={" "}
                        id="phone_number"
                        placeholder="Телефон"
                        type="text"
                        value={values.phone_number}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={requestErrors.phone_number || touched.phone_number}
                        error={errors.phone_number}
                        label={''}
                    />
                    <Input
                        id="password"
                        placeholder="Код пароль"
                        type="text"
                        value={values.password}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        touched={touched.password}
                        error={errors.password}
                    />
                    <Button type="submit">
                        Зарегистрироваться
                    </Button>
                </form>
            </div>
        )
    }
}


export default Form;