import * as callMeActionTypes from "./actiontypes";

import {makeActionCreator} from 'api/index'

export const dataIsFetching = makeActionCreator(callMeActionTypes.DATA_IS_FETCHING, 'bool');
export const callMeBack = makeActionCreator(callMeActionTypes.CALLMEBACK, 'data');
export const callMeBackSuccess = makeActionCreator(callMeActionTypes.CALLMEBACK_SUCCESS, 'data');
