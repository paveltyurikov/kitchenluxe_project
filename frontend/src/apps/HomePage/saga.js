import {call, put, takeLatest} from 'redux-saga/effects'
import Api from '../../api/pageblocks'
import * as pageblocksActionTypes from './actiontypes'
import * as pageblocksActions from './actions'



export function* fetchPageBlocks(action) {
    try {
        const data = yield call(Api.fetchPageBlocks, action);

        yield put(pageblocksActions.fetchPageBlocksSuccess(data));
    } catch (error) {
        console.log(error.message);
        yield put(pageblocksActions.dataIsFetching(false))
    }
}


export default function* pageblocksSaga() {
    yield takeLatest(pageblocksActionTypes.FETCH_PAGE_BLOCKS, fetchPageBlocks);
}

