import React, {Component} from 'react';

import {bindActionCreators, compose} from "redux";
import {connect} from "react-redux";
import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import reducer from './reducer';
import saga from './saga';

import * as actions from "./actions";

import HomePagesSeriesItem from '../../components/Layout/HomePageSeriesItem'
import PageBlock from '../../components/Layout/PageBlock'
import Carousel from '../../components/Carousel/index'


class HomePage extends Component {
    componentDidMount() {

    }

    render() {
        const {products, categories, pageblocks} = this.props;
        return (
            <div className={'home-page'}>
                <div className={'series-block'}>
                    {
                        categories &&
                        <Carousel items={categories} Component={HomePagesSeriesItem}/>
                    }
                </div>
                {
                    pageblocks.length > 0 &&
                    pageblocks.map(pageBlock => <PageBlock key={pageBlock.id} {...pageBlock}/>)
                }
            </div>
        )
    }
}

const mapStateToProps = state => (state.pageblocks);
const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

const withReducer = injectReducer({key: 'pageblocks', reducer});
const withSaga = injectSaga({key: 'pageblocks', saga});

const withConnect = connect(
    mapStateToProps,
    mapDispatchToProps,
);

export default compose(
    withReducer,
    withSaga,
    withConnect)(HomePage)

