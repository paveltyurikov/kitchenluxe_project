import * as pageblocksActionTypes from './actiontypes';



import {createReducer} from 'api/index'


const pageBlocksInitialState =
    {
        pageblocks: [],
    };

const homePageReducer = createReducer(pageBlocksInitialState, {
    [pageblocksActionTypes.FETCH_PAGE_BLOCKS_SUCCESS](state, action) {
        return {
                ...state,
                pageblocks: action.data
            };
    }
});

export default homePageReducer;

