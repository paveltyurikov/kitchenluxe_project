import * as pageblocksActionTypes from './actiontypes';

import {makeActionCreator} from 'api/index'
export const dataIsFetching = makeActionCreator(pageblocksActionTypes.DATA_IS_FETCHING, 'bool');
export const fetchPageBlocks = makeActionCreator(pageblocksActionTypes.FETCH_PAGE_BLOCKS);
export const fetchPageBlocksSuccess = makeActionCreator(pageblocksActionTypes.FETCH_PAGE_BLOCKS_SUCCESS, 'data');