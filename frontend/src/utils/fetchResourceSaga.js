import {call, put, takeEvery} from 'redux-saga/effects'

export function* fetchResourceSaga(
    {
        ApiCall,
        action,
        fetchSuccessAction,
        fetchFailedAction,
        logToConsole=false

    }) {
    try {
        const data = yield call(ApiCall, action);
        yield put(fetchSuccessAction(data))
    } catch
        (error) {
        logToConsole && console.log(error);
        if (error.response &&
            error.response.format === 'json') yield put(
            fetchFailedAction(error.response.data)
        )
    }
}