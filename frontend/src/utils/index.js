import {call, put, takeEvery} from 'redux-saga/effects'

export const isDevEnv = () => process.env.NODE_ENV !== 'production';

export function* fetchResourceSaga(
    {
        ApiCall,
        action,
        fetchSuccessAction,
        fetchFailedAction,
        logToConsole = false

    }) {
    try {
        const data = yield call(ApiCall, action);
        yield put(fetchSuccessAction(data))
    } catch
        (error) {
        logToConsole && console.log(error);
        if (error.response &&
            error.response.format === 'json') yield put(
            fetchFailedAction(error.response.data)
        )
    }
}

export const makeActionCreator = (type, ...argNames) => {
    return function (...args) {
        let action = {type};
        argNames.forEach((arg, index) => {
            action[argNames[index]] = args[index]
        });
        return action
    }
};

export const createReducer = (initialState, handlers) => {
    return (state = initialState, action) => {
        if (handlers.hasOwnProperty(action.type)) {
            return handlers[action.type](state, action)
        } else {
            return state
        }
    }
};


export const parseCookies = () => {
    let cookies = {};
    if (document.cookie && document.cookie !== '') {
        document.cookie.split(';').forEach(function (c) {
            let m = c.trim().match(/(\w+)=(.*)/);
            if (m !== undefined) {
                cookies[m[1]] = decodeURIComponent(m[2]);
            }
        });
    }
    return cookies;
};

export const getCsrfToken = () => {
    const cookies = parseCookies();
    return cookies.csrftoken
};

export const getSessionId = () => {
    const cookies = parseCookies();
    return cookies.sessionid
};

export const getHeaders = (contentType = 'application/json') => {
    //contentType == 'multipart/form-data'
    return contentType === 'multipart/form-data' ?
        {
            'X-CSRFToken': getCsrfToken(),
        }
        :
        {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': contentType,
            'X-CSRFToken': getCsrfToken(),
        }

};

export const headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
};

export const getFormData = data => {
    const formData = new FormData();
    Object.keys(data).forEach(key => formData.append(key, data[key]));
    return formData;
};


export const getParentPath = location => {
    //TODO Пересмотреть функцию при возможности
    // Пытаемся найти титл верхнего раздела
    const paths = location.pathname.split('/');
    return paths.length > 0 && paths[0] === '' ? `/${paths[1]}` : `/${paths[0]}`
};

export const getClearedParentPath = location => {
    return getParentPath(location) === "/" ? 'main-none' : getParentPath(location).replace('/', '');
};

export const numberInArray = (array, number) => array.filter(el => el === number).length > 0;

export const sortBy = (array, key) => {
    return array.sort(function (a, b) {
        const x = a[key];
        const y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
};

export const arraysHasEqualLength = (array1, array2) => {
    return array1.length === array2.length
};

export const cloneArray = array => JSON.parse(JSON.stringify(array));