// models should be registered in some settings for generations default reducers for default actions
// So for RESTfull application we can achieve kind a ORM for data state
request = {
    create: (url, data) => {
        console.log('post')
    },
    retrieve: (url, data) => {
        console.log('retrieve')
    },
    update: (url, data) => {
        console.log('update')
    },
    delete: (url, data) => {
        console.log('delete')
    },
};

export class Model {
    // Class used in React Apps for talking to REST API with redux and redux-saga
    constructor(model) {
        this.model = model;
        this.url = `/api/${model}`;
        //gets the values or options for initializing permissions and other staff
        // Benefits:
        // We still have to define actions sagas api and reducer (especially reducers)
        // we can generate js files with bash, node, python or whatever scripts
        // So here we totally can dispatch  some actions dynamically
    }

    makeAction = (permissions, action, data) => {
        this.checkPermissions(permissions, action) && request[action](this.url, data);
        console.log('Model.',action,'()')
    };
    // Class for managing models on REST points
    create = (permissions, data) => {
        // Fires actions.create('Model', 'data')
        // Action creator as a common function generates "CREATE" action with ('model' , data)
        // Saga catches "CREATE" and call FetchRequest based on 'model'
        // Saga backing response to reducer with "CREATE_RESPONSE" model, status, data
        // Single Saga generates Action "MODEL_CREATE_[SUCCESS, FAILED]", data
        // Reducer changes or not state[model] staff
        this.makeAction(permissions, 'create', data)

    };
    retrieve = (permissions, data) => {
        this.makeAction(permissions, 'retrieve', data)
    };
    update = (permissions, data) => {
        this.makeAction(permissions, 'update', data)
    };
    delete = (permissions, data) => {
        this.makeAction(permissions, 'delete', data)
    };
    checkPermissions = (permissions, action) => {
        console.log(permissions, action);
        return true
    }
}