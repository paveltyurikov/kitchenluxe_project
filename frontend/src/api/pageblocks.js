import {PAGE_BLOCKS} from './paths'

const parse_cookies = () => {
    let cookies = {};
    if (document.cookie && document.cookie !== '') {
        document.cookie.split(';').forEach(function (c) {
            let m = c.trim().match(/(\w+)=(.*)/);
            if (m !== undefined) {
                cookies[m[1]] = decodeURIComponent(m[2]);
            }
        });
    }
    return cookies;
};
const cookies = parse_cookies();
const csrftoken = cookies.csrftoken;

const headers = {
    'Accept': 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
    'X-CSRFToken': csrftoken,
};

const headers_multipart = {
    'Accept': 'application/json, text/plain, */*',
};

const Api = {
    fetchPageBlocks: async (action) => {
        const response = await fetch(

            PAGE_BLOCKS,
            {
                method: 'GET',
                headers: headers,
                //credentials: "same-origin",
            });
        return await response.json();
    },
};

export default Api;