export default createFormData = (obj) => {
    let formData = new FormData();
    Object.keys(obj).map(key => {
            if (key === 'options') {
                return null
            }
            formData.append('csrfmiddlewaretoken', csrftoken)
            return formData.append(key, obj[key])
        }
    );
    return formData
};