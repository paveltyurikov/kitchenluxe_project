/**
 * Create the store with dynamic reducers
 */

import {createStore, applyMiddleware, compose} from 'redux';
import {routerMiddleware} from 'react-router-redux';
import createSagaMiddleware from 'redux-saga';
import createReducer from './reducers.js'
import {isDevEnv} from 'utils'

const sagaMiddleware = createSagaMiddleware();

//const isDevEnv = () => process.env.NODE_ENV !== 'production';


export default function configureStore(initialState = {}, history) {
    // Create the store with two middlewares
    // 1. sagaMiddleware: Makes redux-sagas work
    // 2. routerMiddleware: Syncs the location/URL path to the state
    const middlewares = [
        sagaMiddleware,
        routerMiddleware(history),
    ];

    const enhancers = [
        applyMiddleware(...middlewares),
    ];
    const store = createStore(
        //rootReducer,
        createReducer(),
        initialState,
        compose(
            ...enhancers,
            window.devToolsExtension && isDevEnv() ? window.devToolsExtension() : f => f
            /*window.devToolsExtension  ? window.devToolsExtension() : f => f*/
        )
    );

    // Extensions
    store.runSaga = sagaMiddleware.run;
    store.injectedReducers = {}; // Reducer registry
    store.injectedSagas = {}; // Saga registry

    // Make reducers hot reloadable, see http://mxs.is/googmo
    /* istanbul ignore next */
    if (module.hot) {
        module.hot.accept('./reducers', () => {
            store.replaceReducer(createReducer(store.injectedReducers));
        });
    }

    return store;
}
