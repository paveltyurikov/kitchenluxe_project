import {combineReducers} from 'redux';
import {routerReducer} from 'react-router-redux';
import basket from 'apps/Basket/reducer'
export default function createReducer(injectedReducers) {
    return combineReducers({
        routing: routerReducer,
        basket: basket,
        ...injectedReducers,
    });
}