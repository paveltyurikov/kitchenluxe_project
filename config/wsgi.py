import os
import sys

from django.core.wsgi import get_wsgi_application

# This allows easy placement of apps within the interior
# pampampam_project directory.
app_path = os.path.dirname(os.path.abspath(__file__)).replace('/config', '')
sys.path.append(os.path.join(app_path, 'apps'))

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "config.settings.production")
application = get_wsgi_application()