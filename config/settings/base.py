"""
Base settings for kitchenluxe_project project.
"""

from oscar import OSCAR_MAIN_TEMPLATE_DIR
from oscar import get_core_apps
import environ

ROOT_DIR = environ.Path(__file__) - 3
APPS_DIR = ROOT_DIR.path('apps')

env = environ.Env()
env_file = str(ROOT_DIR.path('.env'))
env.read_env(env_file)

DJANGO_APPS = [
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.flatpages',
    'django.contrib.admin',
]

THIRD_PARTY_APPS = [
    'easy_thumbnails',
    'crispy_forms',
    'widget_tweaks',
    'rest_framework',
    'django_filters',
    'oscarapi',
]

PROJECT_APPS = [
    'core',
    'callmeback',
]


INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + PROJECT_APPS + get_core_apps([

    'custom_apps.basket',
    'custom_apps.catalogue',
    'custom_apps.checkout',
    'custom_apps.order',
    'custom_apps.promotions',
    'custom_apps.dashboard',
    'custom_apps.dashboard.catalogue',

])

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.contrib.flatpages.middleware.FlatpageFallbackMiddleware',
    'oscarapi.middleware.ApiBasketMiddleWare',
]

DEBUG = env.bool('DJANGO_DEBUG', False)

FIXTURE_DIRS = (
    str(APPS_DIR.path('fixtures')),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': str(ROOT_DIR.path('db.sqlite3')),
        'ATOMIC_REQUESTS': True
    }
}


TIME_ZONE = 'Europe/Moscow'
LANGUAGE_CODE = 'ru-ru'
SITE_ID = 1
USE_I18N = True
USE_L10N = True
USE_TZ = True

TEMPLATES = [
    {

        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            str(APPS_DIR.path('templates')),
            OSCAR_MAIN_TEMPLATE_DIR
        ],
        'OPTIONS': {
            'debug': DEBUG,
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

CRISPY_TEMPLATE_PACK = 'bootstrap3'

STATICFILES_DIRS = [
    ROOT_DIR('static'),
]

STATICFILES_FINDERS = [
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
]

STATIC_ROOT = str(ROOT_DIR('public/static'))
STATIC_URL = '/static/'

MEDIA_ROOT = str(ROOT_DIR('public/media'))
MEDIA_URL = '/media/'

ROOT_URLCONF = 'config.urls'

WSGI_APPLICATION = 'config.wsgi.application'

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

AUTHENTICATION_BACKENDS = [
    'oscar.apps.customer.auth_backends.EmailBackend',
    'django.contrib.auth.backends.ModelBackend',
]

ADMIN_URL = r'^config/'

REST_FRAMEWORK = {
    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',

    ),
    'DEFAULT_PARSER_CLASSES': (
        'rest_framework.parsers.JSONParser',
        'rest_framework.parsers.FormParser',
        'rest_framework.parsers.MultiPartParser',
    ),
}

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
}

RBMQ_UESR = "mi_user"
RBMQ_PASSWORD = "mi_password314159265359"
RBMQ_VHOST = "mi_vhost"
BROKER_URL = "amqp://{}:{}@localhost:5672/{}".format(RBMQ_UESR, RBMQ_PASSWORD, RBMQ_VHOST)

TELEGRAM_CHAT_ID = "-191528857"
BOT_TOKEN = "330438622:AAF4xjO-Y-JdGWuAGzCM43nXEHjRxEFU-pM"

THUMBNAIL_ALIASES = {
    '': {
        'product_list_320_480': {
            'size': (320, 480),
            'quality': 90,
            'crop': 'scale'
        },
        'product_list_253_380': {
            'size': (253, 380),
            'quality': 90,
            'crop': 'scale'
        },
    },
}

WEBPACK = False


from .oscar_settings import * #noqa
