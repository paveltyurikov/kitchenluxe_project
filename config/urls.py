from django.conf import settings
from django.conf.urls import include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import TemplateView

from apps.custom_api.app import application as api
#from oscarapi.app import application as api
from core.views import PageBlockView
from custom_apps.app import application
from callmeback.views import CallMeBackCreateView

index_template = 'index.html'

if settings.WEBPACK:
    index_template = 'webpack_loader.html'

urlpatterns = [
                  url(settings.ADMIN_URL, admin.site.urls),
                  url(r'^oscar/', include(application.urls)),
                  url(r'^api/pageblocks/', PageBlockView.as_view(), name='page-block-api-view'),
                  url(r'^api/', include(api.urls)),
                  url(r'^api/callback/', CallMeBackCreateView.as_view(), name='call-me-back'),
                  url(r'^/i18n/', include('django.conf.urls.i18n')),


              ] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + static(
                settings.STATIC_URL, document_root=settings.STATIC_ROOT) + [url(r'^.*$', TemplateView.as_view(template_name=index_template))]
