from django.db import models
from oscar.models.fields import PhoneNumberField

REASON_CHOICES=(
    ('reply', 'I have a reply'),
    ('question', 'I have a question'),
    ('suggestion', 'I have a suggestion'),
)

class CallMeBackRequest(models.Model):
    name = models.CharField(verbose_name="Your name", help_text='Please, give us your name, so we can ask you personally', max_length=255)
    reason = models.CharField(verbose_name="Reason", choices=REASON_CHOICES, max_length=50, blank=True)
    email = models.EmailField(verbose_name="Email address", help_text='Give us an email or phone number or both', blank=True)
    phone = PhoneNumberField(verbose_name='Phone number', help_text='Give us an email or phone number or both', blank=True)
    text = models.TextField(verbose_name="Message", help_text='You can write us a message, or just leave contacts', blank=True)

    def __str__(self):
        return "{} {} {}".format(self.name, self.email, self.phone)
