import re

from django.conf import settings
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse_lazy
from django.views.generic import CreateView
from rest_framework import viewsets, status
from rest_framework.response import Response
from core.tasks import bot_send_message
from .serializers import CategorySerializer
from .forms import CallMeBackForm
from .models import CallMeBackRequest


class CallMeBackViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = CallMeBackRequest.objects.all()

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data, many=True)
        if serializer.is_valid():
            serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED,
                            headers=headers)
        errors = {}
        for error in serializer.errors:
            for key in error.keys():
                if key == 'poll_option':
                    return Response({"error": "Голосование закрыто"}, status=status.HTTP_403_FORBIDDEN)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CallMeBackCreateView(CreateView):
    template_name = 'callmeback/callmeback-create-view.html'
    model = CallMeBackRequest
    form_class = CallMeBackForm

    def get_success_url(self):
        post = self.request.POST
        msg = "Thanks for request. We're call you back soon."
        messages.success(self.request, msg, extra_tags='safe noicon')
        next_url = post.get('next_url')

        if next_url:
            return next_url
        else:
            return reverse_lazy('home')

    def form_valid(self, form):
        if form.is_valid():
            if len(re.findall('\d', form.data['name']))<4:
                self.object = form.save()
                message = "Person: {}, email: {}, phone: {}, theme: {}, message: {}".format(
                    self.object.name,
                    self.object.email,
                    self.object.phone,
                    self.object.reason,
                    self.object.text)
                self.send_message(settings.TELEGRAM_CHAT_ID, message)

        return HttpResponseRedirect(self.get_success_url())

    def send_message(self, CHAT_ID, message):
        bot_send_message(CHAT_ID=CHAT_ID, message=message)
