from django.contrib import admin

from .models import CallMeBackRequest


@admin.register(CallMeBackRequest)
class CallBackRequest(admin.ModelAdmin):
    list_display = ('name', 'email', 'phone',)
