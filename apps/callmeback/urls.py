from django.conf.urls import url

from .views import *

app_name = 'callmeback'
urlpatterns = [
    url(r'^callmeback/$', CallMeBackCreateView.as_view(), name='callmeback-crete'),
]