from django.forms import ModelForm

from .models import CallMeBackRequest


class CallMeBackForm(ModelForm):
    class Meta:
        model = CallMeBackRequest
        exclude = ()
