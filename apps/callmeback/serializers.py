from rest_framework import serializers

from callmeback.models import CallMeBackRequest


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = CallMeBackRequest
        exclude = ()