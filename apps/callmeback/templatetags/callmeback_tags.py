from django import template

from callmeback.forms import CallMeBackForm

register = template.Library()


@register.inclusion_tag('callmeback/callmeback-form.html')
def render_callback_form(request=None):
    form = CallMeBackForm
    if request:
        return {'form': form, 'request': request}
    else:
        return {'form': form}
