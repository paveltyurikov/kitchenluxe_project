from oscarapi.app import RESTApiApplication
from django.conf.urls import url


from .urls import urlpatterns


class CustomRESTApiApplication(RESTApiApplication):

    # def get_urls(self):
    #     urls = [url(
    #         r'^cat/$',
    #         FullProductInfoView.as_view(), name='product-list')]
    #     return urls + super(CustomRESTApiApplication, self).get_urls()
    def get_urls(self):
        return self.post_process_urls(urlpatterns)


application = CustomRESTApiApplication()
