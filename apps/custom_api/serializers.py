from easy_thumbnails.files import get_thumbnailer
from oscar.core.loading import get_class
from oscarapi.serializers.basket import BasketSerializer as CoreBasketSerializer
from oscarapi.serializers.basket import LineAttributeSerializer as CoreLineAttributeSerializer
from oscarapi.serializers.basket import LineSerializer as CoreLineSerializer
from oscarapi.serializers.checkout import PriceSerializer
from oscarapi.serializers.product import ProductImageSerializer as CoreProductImageSerializer
from oscarapi.serializers.product import ProductSerializer as CoreProductSerializer
from rest_framework import serializers
from custom_apps.catalogue.models import ProductImage, CategoryImage

from custom_apps.basket.models import Line
from custom_apps.catalogue.models import OptionChoice, Option, Category

Selector = get_class('partner.strategy', 'Selector')




class CategorySerializer(serializers.ModelSerializer):
    parent = serializers.SerializerMethodField()
    image = serializers.SerializerMethodField()
    class Meta:
        model = Category
        exclude = ()

    def get_parent(self, obj):
        try:
            return obj.get_parent().id
        except:
            return None

    def get_image(self, obj):
        try:
            return obj.image.url
        except:
            return None


class OptionChoiceSerializer(serializers.ModelSerializer):
    class Meta:
        model = OptionChoice
        exclude = ()


class OptionSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='option-detail')
    choices = OptionChoiceSerializer(many=True, read_only=True)

    class Meta:
        model = Option
        fields = ('id', 'url', 'name', 'code', 'is_required', 'choices')


class ProductImageSerializer(CoreProductImageSerializer):
    product_list_320_480 = serializers.SerializerMethodField()
    product_list_253_380 = serializers.SerializerMethodField()

    class Meta:
        model = ProductImage
        exclude = ()

    def get_product_list_320_480(self, obj):
        return get_thumbnailer(obj.original)['product_list_320_480'].url
    def get_product_list_253_380(self, obj):
        return get_thumbnailer(obj.original)['product_list_253_380'].url


class ProductSerializer(CoreProductSerializer):
    price = serializers.SerializerMethodField()
    options = OptionSerializer(many=True, required=False)
    images = ProductImageSerializer(many=True, required=False)

    def get_price(self, obj):
        request = self.context.get("request")
        strategy = Selector().strategy(
            request=request, user=request.user)
        ser = PriceSerializer(
            strategy.fetch_for_product(obj).price,
            context={'request': request})
        return ser.data


class LineAttributeSerializer(CoreLineAttributeSerializer):
    option = OptionSerializer(many=False)


class LineSerializer(CoreLineSerializer):
    product = ProductSerializer(many=False, read_only=True)
    edge = serializers.SerializerMethodField()
    width = serializers.SerializerMethodField()
    height = serializers.SerializerMethodField()
    attributes = LineAttributeSerializer(
        many=True,
        fields=('url', 'option', 'value'),
        required=False,
        read_only=True)
    line_total_price_incl_tax = serializers.SerializerMethodField()

    class Meta:
        model = Line
        exclude = ()

    def get_line_total_price_incl_tax(self, obj):
        return '{0:.2f}'.format(obj.line_total_price_incl_tax)

    def get_edge(self, obj):
        try:
            return obj.attributes.filter(option__code="edge")[0].value
        except:
            return None

    def get_width(self, obj):
        try:
            return obj.attributes.filter(option__code="width")[0].value
        except:
            return None

    def get_height(self, obj):
        try:
            return obj.attributes.filter(option__code="height")[0].value
        except:
            return None


class BasketSerializer(CoreBasketSerializer):
    total_total = serializers.SerializerMethodField()

    def get_total_total(self, obj):
        return '{0:.2f}'.format(obj.total_total)
