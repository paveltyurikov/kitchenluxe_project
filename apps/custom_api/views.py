from oscarapi.views import basic, basket

from rest_framework import generics
from rest_framework.filters import SearchFilter

from django_filters.rest_framework import DjangoFilterBackend
from .serializers import LineSerializer, ProductSerializer, CategorySerializer
from .pagination import CutHttpPagination
from custom_apps.catalogue.models import Category
from custom_apps.basket.models import Line


class FullProductInfoView(basic.ProductList):
    serializer_class = ProductSerializer
    filter_backends = (DjangoFilterBackend,SearchFilter)
    filter_fields = ('categories__id', 'categories__name')
    search_fields = ('title', 'categories__name')
    pagination_class = CutHttpPagination
    

class FullBasketLineView(basket.LineList):
    def get_serializer_class(self):
        return LineSerializer

class CategoriesView(generics.ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer

