from rest_framework.response import Response
from rest_framework import pagination


class CutHttpPagination(pagination.PageNumberPagination):
    page_size = 12
    max_page_size = 100
    def get_paginated_response(self, data):
        next_link, previous_link = self.cut_http_from_url(self.get_next_link()), \
                                   self.cut_http_from_url(self.get_previous_link())
        return Response({
            'next': next_link,
            'previous': previous_link,
            'count': self.page.paginator.count,
            'results': data
        })

    def cut_http_from_url(self, link):
        if link:
            return  "//{}".format(link.split('://')[-1])
        return None
