from django.db import models
from oscar.models.fields import PhoneNumberField


class PageBlock(models.Model):
    name = models.CharField(verbose_name="Your name",
                            help_text='Name of block', max_length=255)
    url = models.CharField(verbose_name="URL", max_length=255, help_text="URL Where block appears", blank=True,
                           null=True)
    background_image = models.ImageField(upload_to='backgrounds/', blank=True, null=True)
    background_color = models.CharField(verbose_name='Background Color', max_length=20, help_text="HEX or RGBA color")
    text = models.TextField(verbose_name="Text of block",
                            help_text='You can write us a message, or just leave contacts', blank=True)
    sort_idx = models.SmallIntegerField(verbose_name="Sort index", default=0)
    inverse = models.BooleanField(verbose_name='Inverse colors?', default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Page block"
        verbose_name_plural = "Page blocks"
