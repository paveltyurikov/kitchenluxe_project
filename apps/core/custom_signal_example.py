#signals.py

from django.dispatch import Signal
user_login = Signal(providing_args=["request", "user"])


#views.py

from foo import signals

def login(request):
    ...
    if request.user.is_authenticated():
        signals.user_login.send(sender=None, request=request, user=request.user)


#tasks.py

from foo.signals import user_login

def user_login_handler(sender, **kwargs):
    """signal intercept for user_login"""
    user = kwargs['user']
    ...

user_login.connect(user_login_handler)