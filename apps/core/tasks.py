from decimal import Decimal
import datetime
from django.conf import settings

from config.settings.celery import app
from apps.core.telebot import bot

@app.task
def test(bal):
    with open('/opt/projects/mi_project/test.txt', 'a') as f:
        for i in range(0, 100000):
            f.write("{} - {}\n".format(bal, datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")))

@app.task
def bot_callback(sender, order, user, **kwargs):
    message = "order: <b>{}</b>\ndate: {} total: <b>{}</b>\nuser: <b>{}</b>".format(
        order.number,
        order.date_placed.strftime("%d.%m.%Y %H:%M"),
        '{0: .4g}'.format(Decimal(order.total_incl_tax)),
        user.email)
    bot.send_message(
        settings.TELEGRAM_CHAT_ID,
        message,
        parse_mode='HTML'
    )
    print(order, user)

@app.task
def bot_send_message(message, CHAT_ID, **kwargs):
    bot.send_message(
        CHAT_ID,
        message,
        parse_mode='HTML'
    )