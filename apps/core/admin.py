from django.contrib import admin
from .models import PageBlock

@admin.register(PageBlock)
class PageBlockAdmin(admin.ModelAdmin):
    model = PageBlock