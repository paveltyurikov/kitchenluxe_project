from rest_framework import views
from rest_framework.response import Response

from .models import PageBlock
from .serializers import PageBlockSerializer


class PageBlockView(views.APIView):

    def get(self, request, format=None):
        q = PageBlock.objects.all().order_by('sort_idx')
        serializer = PageBlockSerializer(instance=q, many=True)
        return Response(serializer.data)
