from django.core.management.base import BaseCommand

from django.contrib.auth.models import User

class Command(BaseCommand):
    help = 'Closes the specified poll for voting'

    def handle(self, *args, **options):
        self.create_supetuser(username='admin', password='Qwezxc123456')
        self.create_supetuser(username='pavel', password='Qwezxc123456')

    def create_supetuser(self, username, password):
        admin, created = User.objects.get_or_create(username=username)
        if created:
            admin.set_password(password)
            admin.is_superuser = True
            admin.is_staff = True
            admin.save()
