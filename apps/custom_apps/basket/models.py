from decimal import Decimal,ROUND_FLOOR,ROUND_HALF_UP
from django.utils.encoding import smart_text
from oscar.apps.basket.abstract_models import AbstractLine, AbstractBasket

class Basket(AbstractBasket):
    @property
    def total_total(self):
        """
        Return total price inclusive of tax but exclusive discounts
        """
        return self._get_total('line_total_price_incl_tax')

class Line(AbstractLine):
    @property
    def get_square(self):
        objs = self.attributes.filter(option__name__in=["Width", "Height"])
        if len(objs) != 2:
            return None
            # raise MultipleWidhtHeightError("Bad width height values")
        try:
            return Decimal(int(objs[0].value) * int(objs[1].value) * 0.00694444).quantize(Decimal('0.01'))
        except ValueError:
            return Decimal(1)

    @property
    def description(self):
        descr = {'description': None, 'options': {}}
        descr['description'] = smart_text(self.product)
        for attribute in self.attributes.all():
            descr['options'][attribute.option.name] = attribute.value
        return descr

    @property
    def line_price_incl_tax(self):
        if self.get_square:
            if self.get_square > 0:
                return self.quantity * self.unit_price_incl_tax * self.get_square
        return self.quantity * self.unit_price_incl_tax

    @property
    def line_price_excl_tax(self):
        if self.get_square:
            if self.get_square > 0:
             return self.quantity * self.unit_price_excl_tax * self.get_square
        return self.quantity * self.unit_price_excl_tax

    @property
    def line_total_price_incl_tax(self):
        return self.quantity * self.line_price_incl_tax

    @property
    def line_total_price_excl_tax(self):
        return self.quantity * self.line_price_excl_tax


from oscar.apps.basket.models import *  # noqa
