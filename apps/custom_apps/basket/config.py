from oscar.apps.basket import config


class BasketConfig(config.BasketConfig):
    name = 'custom_apps.basket'
