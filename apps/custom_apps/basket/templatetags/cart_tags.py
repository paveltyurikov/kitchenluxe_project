from django import template

from oscar.core.compat import assignment_tag
from oscar.core.loading import get_class, get_model

AddToBasketForm = get_class('custom_apps.basket.forms', 'AddToBasketForm')
Product = get_model('catalogue', 'product')

register = template.Library()

QNT_SINGLE, QNT_MULTIPLE = 'single', 'multiple'


@assignment_tag(register)
def cart_form(request, product):
    if not isinstance(product, Product):
        return ''

    initial = {}
    if not product.is_parent:
        initial['product_id'] = product.id

    form_class = AddToBasketForm
    form = form_class(request.basket, product=product, initial=initial)

    return form