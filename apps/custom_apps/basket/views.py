from oscar.apps.basket.views import BasketAddView as CoreBasketAddView
from .forms import AddToBasketForm

class BasketAddView(CoreBasketAddView):
    form_class = AddToBasketForm