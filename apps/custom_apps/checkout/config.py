from oscar.apps.checkout import config


class CheckoutConfig(config.CheckoutConfig):
    name = 'custom_apps.checkout'

    def ready(self):
        from . import receivers
