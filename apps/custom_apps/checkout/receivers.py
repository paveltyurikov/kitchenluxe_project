from oscar.apps.order.signals import order_placed
from django.dispatch import receiver
from django.conf import settings

#from core.tasks import bot_callback
from apps.core.telebot import bot

def bot_callback(sender, order, user, **kwargs):
    email = order.guest_email
    if not order.is_anonymous and not user.is_anonymous:
        email = user.email
    message = "order: <b>{}</b>\ndate: {} total: <b>{}</b>\nuser: <b>{}</b>".format(
        order.number,
        order.date_placed.strftime("%d.%m.%Y %H:%M"),
        order.total_incl_tax,
        email)
    bot.send_message(
        settings.TELEGRAM_CHAT_ID,
        message,
        parse_mode='HTML'
    )
    print(order, user)

@receiver(order_placed)
def order_placed_callback(sender, order, user, **kwargs):
    bot_callback(sender, order, user, **kwargs)

