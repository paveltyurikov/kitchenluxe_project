from django.shortcuts import redirect
from oscar.apps.checkout.views import PaymentMethodView as CorePaymentMethodView, ShippingAddressView as CoreShippingAddressView
from .forms import ShippingAddressForm

class ShippingAddressView(CoreShippingAddressView):
    form_class = ShippingAddressForm


class PaymentMethodView(CorePaymentMethodView):
    def get_success_response(self):
        return redirect('checkout:preview')
