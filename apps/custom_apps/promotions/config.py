from oscar.apps.promotions import config


class PromotionsConfig(config.PromotionsConfig):
    name = 'custom_apps.promotions'
