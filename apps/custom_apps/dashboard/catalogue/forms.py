from django import forms
from django.forms import inlineformset_factory
from treebeard.forms import movenodeform_factory
from custom_apps.catalogue.models import Category, CategoryImage, Application

CategoryForm = movenodeform_factory(
    Category,
    fields=['name', 'description', 'image',])



class CategoryImageForm(forms.ModelForm):
    class Meta:
        model = CategoryImage
        exclude = ()

class ApplicationForm(forms.ModelForm):
    class Meta:
        model = Application
        exclude = ('category', 'product')


CategoryImageFormSet = inlineformset_factory(
    Category, Application, form=ApplicationForm, extra=2)