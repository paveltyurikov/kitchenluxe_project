from django.utils.translation import ugettext_lazy as _
from django.http import HttpResponseRedirect, Http404, HttpResponse
from oscar.apps.dashboard.catalogue.views import CategoryCreateView as CoreCategoryCreateView
from oscar.apps.dashboard.catalogue.views import CategoryUpdateView as CoreCategoryUpdateView
from .forms import CategoryImageFormSet

class CategoryCreateView(CoreCategoryCreateView):
    def get_context_data(self, **kwargs):
        ctx = super(CategoryCreateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Add a new category")
        if self.request.POST:
            ctx['category_images'] = CategoryImageFormSet(self.request.POST, self.request.FILES)
        else:
            ctx['category_images'] = CategoryImageFormSet()
        return ctx

    def form_valid(self, form):
        context = self.get_context_data()
        category_images = context['category_images']
        if form.is_valid():
            self.object = form.save()
            category_images.instance = self.object
            if category_images.is_valid():
                category_images.save()
        return HttpResponseRedirect(self.get_success_url())


class CategoryUpdateView(CoreCategoryUpdateView):
    def get_context_data(self, **kwargs):
        ctx = super(CategoryUpdateView, self).get_context_data(**kwargs)
        ctx['title'] = _("Update category '%s'") % self.object.name
        if self.request.POST:
            ctx['category_images'] = CategoryImageFormSet(self.request.POST, self.request.FILES, instance=self.object)
        else:
            ctx['category_images'] = CategoryImageFormSet(instance=self.object)
        return ctx

    def form_valid(self, form):
        context = self.get_context_data()
        category_images = context['category_images']
        if category_images.is_valid() and form.is_valid():
            form.save()
            category_images.save()
        return HttpResponseRedirect(self.get_success_url())