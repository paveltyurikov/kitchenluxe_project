from django.utils.encoding import smart_text
from oscar.apps.order.abstract_models import AbstractLine


class Line(AbstractLine):
    @property
    def description(self):
        descr = {'d':None, 'ops':{}}
        descr['d'] = smart_text(self.product)
        for attribute in self.attributes.all():
            descr['ops'][attribute.option.name]= attribute.value

        return descr


from oscar.apps.order.models import *  # noqa
