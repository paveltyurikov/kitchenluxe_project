import json

import requests as rq
from lxml import html

http_proxy = "http://23.88.228.21:8088"

proxyDict = {
    "http": http_proxy
}

url = 'http://www.lioher.com/products/luxe-digital/'


def get_cat_img(tree):
    try:
        return tree.xpath('//div[contains(@class, "vc_row-has-fill")]/@data-vc-parallax-image')[0]
    except IndexError:
        return "No Image"


def get_title(tree):
    try:
        return tree.find(".//title").text.split('|')[0].strip()
    except IndexError:
        return 'No Title'


with open('/opt/projects/mi_project/mi/links.txt', 'r') as f:
    links = f.read().split('\n')

if __name__ == '__main__':
    with open('/opt/projects/mi_project/mi/data.txt', 'a') as d:
        for link in links:
            if len(link) > 0:
                page = rq.get(link, proxies=proxyDict)
                tree = html.fromstring(page.content)
                item = {}
                item['link']=link
                item['img'] = get_cat_img(tree)
                item['title'] = get_title(tree)
                d.write(json.dumps(item)+'\n')
