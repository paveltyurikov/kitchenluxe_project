from oscar.apps.catalogue.admin import *  # noqa
from custom_apps.catalogue.models import Option, OptionChoice, CategoryFile

admin.site.unregister(Option)
class OptionChoiceInline(admin.TabularInline):
    model = OptionChoice

class OptionAdmin(admin.ModelAdmin):
    list_display = ('name', 'id')
    inlines = [OptionChoiceInline, ]

class CategoryFileAdmin(admin.ModelAdmin):
    model = CategoryFile
    fields = ['file', 'title', 'description', 'category']

admin.site.register(Option, OptionAdmin)
admin.site.register(CategoryFile, CategoryFileAdmin)