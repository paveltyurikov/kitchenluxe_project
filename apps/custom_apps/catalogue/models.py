from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db import models
from easy_thumbnails.fields import ThumbnailerImageField


from oscar.apps.catalogue.abstract_models import (
    AbstractCategory,
    AbstractProduct,
    AbstractOption,
    AbstractProductImage)


class Product(AbstractProduct):
    outer_url = models.CharField(
        verbose_name='Outer url',
        max_length=150,
        blank=True)

    class Meta:
        ordering = ['categories__path']


class ProductImage(AbstractProductImage):
    original = ThumbnailerImageField(
        _("Original"), upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255)

class Option(AbstractOption):
    @property
    def has_choices(self):
        if self.choices.count() > 0:
            return True
        else:
            return False


class OptionChoice(models.Model):
    option = models.ForeignKey(Option, verbose_name="Product option", related_name='choices')
    choice = models.CharField(
        verbose_name="Option Choice",
        max_length=120,
        help_text="Define choice from option ex: Orange choice from Color option")

    def __str__(self):
        return self.choice


class Category(AbstractCategory):
    inverse = models.BooleanField(verbose_name='Inverse colors', default=False)
    @property
    def product_count(self):
        return self.product_set.count()


class CategoryImage(models.Model):
    """
    An image of a Category
    """
    category = models.ForeignKey(
        'Category',
        on_delete=models.CASCADE,
        related_name='images',
        verbose_name=_("Category"))
    original = models.ImageField(
        _("File"), upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255)
    caption = models.CharField(_("Caption"), max_length=200, blank=True)

    #: Use display_order to determine which is the "primary" image
    display_order = models.PositiveIntegerField(
        _("Display order"), default=0,
        help_text=_("First is first"))
    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    class Meta:
        app_label = 'catalogue'
        # Any custom models should ensure that this ordering is unchanged, or
        # your query count will explode. See AbstractCategory.primary_image.
        ordering = ["display_order"]
        verbose_name = _('Category Imamge')
        verbose_name_plural = _('Catgory Images')

    def __str__(self):
        return u"Изображение для '%s'" % self.category

    def is_primary(self):
        """
        Return bool if image display order is 0
        """
        return self.display_order == 0

    def delete(self, *args, **kwargs):
        """
        Always keep the display_order as consecutive integers. This avoids
        issue #855.
        """
        super(CategoryImage, self).delete(*args, **kwargs)
        for idx, image in enumerate(self.category.images.all()):
            image.display_order = idx
            image.save()


class Application(models.Model):
    """
    An image of a Category
    """
    category = models.ForeignKey(
        'Category',
        on_delete=models.CASCADE,
        related_name='applications',
        verbose_name=_("Category"),
        blank=True,
        null=True)

    product = models.ForeignKey(
        'Product',
        on_delete=models.CASCADE,
        related_name='applications',
        verbose_name=_("Product"),
        blank=True,
        null=True)

    original = models.ImageField(
        _("Iamge"), upload_to=settings.OSCAR_IMAGE_FOLDER, max_length=255)

    description = models.TextField(_("Description"), max_length=200, blank=True)

    #: Use display_order to determine which is the "primary" image
    display_order = models.PositiveIntegerField(
        _("Display order"), default=0,
        help_text=_("First is first"))
    date_created = models.DateTimeField(_("Date created"), auto_now_add=True)

    class Meta:
        app_label = 'catalogue'
        # Any custom models should ensure that this ordering is unchanged, or
        # your query count will explode. See AbstractCategory.primary_image.
        ordering = ["display_order"]
        verbose_name = _('Category Application')
        verbose_name_plural = _('Catgory Applications')

    def __str__(self):
        return u"Applications for '%s'" % self.category


class CategoryFile(models.Model):
    category = models.ManyToManyField(Category, verbose_name='Category', related_name='category_files')
    title = models.CharField(verbose_name='Title', max_length=150, blank=True, null=True)
    description = models.TextField(verbose_name='Description', blank=True, null=True)
    file = models.FileField(verbose_name='File', upload_to='category_files/')

    class Meta:
        verbose_name = _('Category File')
        verbose_name_plural = _('Category Files')

    def __str__(self):
        return "id:{} {}".format(self.pk, self.title)


from oscar.apps.catalogue.models import *  # noqa
