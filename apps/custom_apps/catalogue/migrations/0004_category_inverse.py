# -*- coding: utf-8 -*-
# Generated by Django 1.11.9 on 2018-02-23 14:13
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('catalogue', '0003_auto_20180113_1841'),
    ]

    operations = [
        migrations.AddField(
            model_name='category',
            name='inverse',
            field=models.BooleanField(default=False, verbose_name='Inverse colors'),
        ),
    ]
