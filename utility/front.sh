#!/usr/bin/env bash

read -p 'npm build?: ' COMMIT_MESSAGE
if [ -n "${COMMIT_MESSAGE}" ]; then
    npm run build --prefix frontend &&
    echo "Удаляю apps/static/js/main*.js"
    rm apps/static/js/main*.js
    echo "Копирую frontend/build/static/js/main*.js в apps/static/js/"
    cp frontend/build/static/js/main*.js apps/static/js/
    echo "Копирую frontend/build/index.html в apps/templates/index.htm"
    cp frontend/build/index.html apps/templates/index.html
fi

echo "Запускаю плейбук"
ansible-playbook -i "~/playbooks/inventory/work.inv" ./utility/ansible/front.yaml --ask-become-pass