#!/usr/bin/env bash
rm db.sqlite3
./manage.py makemigrations &&
./manage.py migrate
./manage.py loaddata ../mi_project/mi/all.json
#./manage.py server_init