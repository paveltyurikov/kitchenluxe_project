#!/usr/bin/env bash
npm run build --prefix frontend &&
rm static/js/main*.js
cp frontend/build/static/js/main*.js static/js/
rm static/css/main*.js
cp frontend/build/static/css/main*.js static/css/
cp frontend/build/index.html apps/templates/index.html